﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eventos
{
    public partial class EventoDetalle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var eventoId = Request["id"];
            int id_Evento;
            if (!String.IsNullOrEmpty(eventoId) && int.TryParse(eventoId, out id_Evento))
            //if(!IsPostBack)
            {
                //colocar el id en un campo oculto
                int.TryParse(eventoId, out id_Evento);
                HiddenId.Value = id_Evento.ToString();


                Evento evt = new Evento(id_Evento);


                //extraer el evento solicitado
                //Modulo modulo = new Modulo(1, 1, 1, true, "Modulo I");
                Modulo modulo = new Modulo(id_Evento);
                //Docente docente = new Docente(1, "", "PhD ", "Alvaro Castro", "Ingeniero Comercial");
                Docente docente = new Docente();
                docente.Datos(modulo._idDocente.ToString());
                Evento evento = new Evento(evt, modulo, docente);

                if (!String.IsNullOrEmpty(evento._nombreEvento))
                {
                    Lbl_nombreEvento.Text = evento._nombreEvento;
                    Lbl_Agenda.Text = evento._nombreEvento;
                    Lbl_tipoEvento.Text = evento._tipoEvento;

                    //mostrar la carrera organizadora
                    Carrera carrera = new Carrera(id_Evento);
                    //Response.Write(carrera.Carreras(id_Evento));
                    Lbl_carrera.Text = carrera.nombreCarrera;

                    Img_evento.ImageUrl = ConfigurationManager.AppSettings["url_admin"] + evento._imagen;
                    Lbl_fecha.Text = evento._fecha;
                    if (evento._costo > 0)
                        Lbl_costo.Text = String.Format(" {0:c}", evento._costo); //.ToString();
                    else
                        Lbl_costo.Text = "Gratuito";
                    Lbl_modalidad.Text = evento._modalidad;
                    Lbl_cantidadHoras.Text = evento._cantidadHoras;
                    Lbl_direccion.Text = evento._direccion;

                    //Lbl_informacionEvento.Text = "Descripción del evento: Lorem ipsum....Lorem ipsum<br/>";
                    Lbl_informacionEvento.Text = evento._informacionEvento;
                    // Lbl_grado.Text = evento._docente._grado;
                    //Lbl_nombreDocente.Text = evento._docente._nombreCompleto;
                    //Img_docente.ImageUrl = evento._docente._imagen;

                    DataTable testDt = new Modulo().SetAgenda(id_Evento);

                    ListView1.DataSource = testDt;
                    ListView1.DataBind();
                }
                else 
                {
                    evento_detalle.Attributes.Add("style", "display:none;");
                    Lbl_vacio.Text = "No existe el evento";
                    Lbl_vacio.ForeColor = Color.Red;
                }
            }
            else
            {
                //Debug.Fail("ERROR : No podemos mostrar un evento sin ID");
                //throw new Exception("ERROR : no podemos cargar EventoDetalle ");
                Response.Redirect("Eventos.aspx");
            }

            VerificaFecha();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Response.Redirect("Pre-inscripcion.aspx");
            string url = string.Format("{0}?id={1}", "Pre-inscripcion.aspx", HiddenId.Value);
            Response.Redirect(url);
        }

        private void VerificaFecha()
        {
            try
            {
                coneccion conex = new coneccion();
                string query1 = "select TOP 1 [fechaInicio],horario from [dbo].[evt_Modulo] where idEvento=" + Request.QueryString["id"] + " order by [fechaInicio]";
                using (SqlCommand command1 = new SqlCommand(query1, conex.connection))
                {
                    conex.Abrir();
                    using (SqlDataReader reader = command1.ExecuteReader())
                    {
                        reader.Read();

                        var date = DateTime.Now;
                        string datetime = DateTime.Now.ToString("hh:mm:ss tt");

                        string[] subs = reader.GetString(1).Split('a');
                        string[] hora = subs[0].Split(':');
                        DateTime ejemploFecha = new DateTime(reader.GetDateTime(0).Year, reader.GetDateTime(0).Month, reader.GetDateTime(0).Day, Int32.Parse(hora[0]), Int32.Parse(hora[1]), 0);
                        bool a = false;
                        if (date > ejemploFecha)
                        {
                            a = true;
                        }
                        if (a)
                        {
                            Button1.Enabled = false;
                            //TextCI.Disabled = true;
                            Lbl_VerificarGlobal.Text = "Fecha vencida para preinscripción al Evento<br>";
                            Lbl_VerificarGlobal.ForeColor = Color.Red;
                        }


                    }
                }
            }
            catch (Exception ex) { Response.Write(ex.Message); }

        }
    }
}