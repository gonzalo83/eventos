﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Eventos.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Eventos.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style>
        .img-fluid{
        width: 348px;
        height: 348px !important;
      }

      .card-img, .card-img-top{
            width: 350px;
height: 202px;
opacity: 0.9;
        }
        .card{
            opacity: 1;
        }
        .mostrarArriba{
            animation: mostrarArriba 1.5s;
        }
    
        @keyframes mostrarArriba {
            0%{
                transform: translateY(60px);

            }
            100%{
                transform: translateY(0);
            }
        }
  </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
    
    <div class="container" style="padding:20px 0 0 0">

    <img loading="lazy" src="img/banner.png"/>
  <div class="row">
  
    <div class="col-sm-10">
    <br />
    <h1>Oferta Educativa en capacitación</h1>
   <p>
    Nuestra Casa de Estudios brinda ofertas educativas en actualización y especialización abierta a todo el público, tanto a estudiantes y personas particulares que requieren de programas académicos de la más alta calidad y alineados con las necesidades del mundo de hoy.

Encontrarás opciones educativas de calidad en la búsqueda de beneficiar positivamente a las comunidades, principalmente las profesionales. 
   </p>
    </div>
        

    
</div>
</div>




<section class="pt-5 pb-5">
  <div class="container">
    <div class="row">
        <div class="col-6">
            <h3 class="mb-3">Eventos Nuevos </h3>
        </div>
        <div class="col-6 text-right">
            <a class="btn btn-primary mb-3 mr-1" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                <i class="fa fa-arrow-left">anterior</i>
            </a>
            <a class="btn btn-primary mb-3 " href="#carouselExampleIndicators2" role="button" data-slide="next">
                <i class="fa fa-arrow-right">siguiente</i>
            </a>
        </div>
        <div class="col-12">
            <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">

                           <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>

                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="row">

                            <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>

                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</section>
      
    


<div class="jumbotron text-center">
  <a href="Eventos.aspx"><h1>Elegir evento</h1></a>
</div>


<script>
    let animado = document.querySelectorAll(".card");;
    function mostrarScroll() {
        let scrollTop = document.documentElement.scrollTop;
        console.log(scrollTop);

        for (var i = 0; i < animado.length; i++) {
            let alturaAni = animado[i].offsetTop;
            if (alturaAni - 200 < scrollTop) {
                animado[i].style.opacity = 1;
                animado[i].classList.add("mostrarArriba");
            }
            /* if(scrollTop<=100||scrollTop==0){
                 animado[i].style.opacity=0;
                 animado[i].classList.remove("mostrarArriba");
         }
            */
        }
    }
    window.addEventListener('scroll', mostrarScroll);
</script>


</asp:Content>
