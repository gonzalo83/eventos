﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eventos
{
    public partial class Pre_inscripcion : System.Web.UI.Page
    {
        string cadena_correo="";
        string mensaje_respuesta="";
        protected void Page_Load(object sender, EventArgs e)
        {
            Panel2.Visible = false;
            Panel3.Visible = false;

            var id = Request["id"];
            //Response.Write(id);
            //if (id != null && IsPostBack)
            int id_Evento;
            if (!String.IsNullOrEmpty(id) && int.TryParse(id, out id_Evento))
            {
                IdEvento.Value = id;
                string nombre_evento = new Evento().NombreEvento(id_Evento);
                Lbl_InformacionEvento.Text = "Se preinscribe al Evento: <h4 style=\"color:green;\">"+nombre_evento+"</h4>";
            }
            else
            {
                //Debug.Fail("ERROR : No podemos mostrar un evento sin ID");
                //throw new Exception("ERROR : no podemos cargar EventoDetalle ");
                //Response.Redirect("Eventos.aspx");
            }
        }

        protected void Btn_Verificar_Click(object sender, EventArgs e)
        {
            //Panel1.Visible = false;
            Panel2.Visible = true;
            Lbl_VerificarCI.ForeColor = Color.Green;
            Btn_Verificar.Visible = false;
            TextCI.Disabled = true;
            /************recuperar datos del cliente***************/
            //Preinscripcion preinscripcion = new Preinscripcion();
            //preinscripcion._nuevo = false;
            Estudiante estudiante = new Estudiante(TextCI.Value);
            if (estudiante._idEstudiante>0)//si id existe el estudiante sera mayor a 0
            {
                //preinscripcion._nuevo = false;
                Lbl_VerificarCI.Text = "Usted esta registrado como estudiante. Se han recuperado sus datos.";
                TextNombre.Value = estudiante._nombreCompleto;
                TextNombre.Disabled = true;
                TextPaterno.Value = "C";
                TextPaterno.Disabled = true;
                TextMaterno.Value = "M";
                TextMaterno.Disabled = true;
                TextEmail.Value = "freysner@gmail.com";
                TextEmail.Disabled = true;
                TextCelular.Value = "7";
                TextCelular.Disabled = true;
                TextNacimiento.Value = "31/1/2000";
                TextNacimiento.Disabled = true;
                CheckNuevo.Checked = false;

            }
            else
            {
                Lbl_VerificarCI.Text = "LLene todos sus datos. Su correo y su celular son importantes para poder comunicarnos.";
                CheckNuevo.Checked = true;
            }
        }

        protected async void Btn_Registrar_Click(object sender, EventArgs e)
        {
            Btn_Registrar.Visible = false;
            Preinscripcion preinscripcion = new Preinscripcion();

            //verificar si la persona ya se pre-inscribio en el mismo evento
            bool flag_CI_Evento = true;//falta verificar

            Llenar_datos(preinscripcion);

            string email = TextEmail.Value;
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            bool flag_email=match.Success;
            if (flag_email)
            {
                preinscripcion._correo = email;
                //Response.Write(email + " is correct");
            }
            else
            {
                LabelErrorEmail.Text = "Email invalido";
                LabelErrorEmail.ForeColor = Color.Red;
            }

            if (flag_CI_Evento && flag_email)
            {
                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = true;
                
                //Response.Write(preinscripcion.Save());
                double costo = new Evento().CostoEvento(Convert.ToInt32(IdEvento.Value));

                //if (costo>0 || CheckNuevo.Checked)
                if (!CheckNuevo.Checked)
                {
                    //primero llenar los mensajes
                    if (await preinscripcion.Save())
                    {
                        MensajeEstudiante(CheckNuevo.Checked, costo);
                        //con los mensaje creados, se envia el correo
                        Enviar_correo(TextEmail.Value);
                        lblmensajefinal.Text = mensaje_respuesta;
                    }
                    else
                    {
                        lblmensajefinal.Text = "Lo sentimos mucho, ocurrio un error, intente de nuevo.";
                        lblmensajefinal.ForeColor = Color.Red;
                    }//*/

                }
                else
                {
                   

                    if (await preinscripcion.Save())
                    {
                        MensajeEstudiante(CheckNuevo.Checked, costo);
                        //con los mensaje creados, se envia el correo
                        Enviar_correo(TextEmail.Value);
                        lblmensajefinal.Text = mensaje_respuesta;
                    }
                    else
                    {
                        lblmensajefinal.Text = "Lo sentimos mucho, ocurrio un error, intente de nuevo.";
                        lblmensajefinal.ForeColor = Color.Red;
                    }//*/

                }
            }
            else
            {
                Panel1.Visible = true;
                Panel2.Visible = true;
                Btn_Registrar.Visible = true;
            }
      

        }

        protected void Llenar_datos(Preinscripcion preinscripcion)
        {
            //id de la bb dd tiene que ser correcto
            //preinscripcion._idEvento = 6;
            preinscripcion._idEvento = Convert.ToInt32(IdEvento.Value);

            preinscripcion._fechaPreinscripcion = DateTime.Now.ToLocalTime();
            preinscripcion._cedulaIdentidad = TextCI.Value;
            preinscripcion._nombres = TextNombre.Value;
            preinscripcion._apPaterno = TextPaterno.Value;
            preinscripcion._apMaterno = TextMaterno.Value;
            preinscripcion._fechaNacimiento = Convert.ToDateTime(TextNacimiento.Value);
            preinscripcion._numeroCelular = TextCelular.Value;
            preinscripcion._nuevo = CheckNuevo.Checked;

            //el codigo a generar hay realizarlo
            //Random objRandom = new Random();
            //string captchaText = string.Format("{0:X}", objRandom.Next(1000000, 9999999));
            //Response.Write(captchaText);

        }

        protected void Enviar_correo(string EmailDestino)
        {
            try
            {
                
                //string EmailDestino="freysner@gmail.com";
                string EmailOrigen = "universidad_prueba@outlook.es";
                string Contraseña = "universidad123456";
               
                SmtpClient client = new SmtpClient("smtp-mail.outlook.com"); ;
                // utilizamos el servidor SMTP de gmail
                client.Port = 25;
                //client.Host = Ip;
                client.EnableSsl = true;
                //client.Timeout = 1000000000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                // nos autenticamos con nuestra cuenta de gmail
                client.Credentials = new NetworkCredential(EmailOrigen, Contraseña);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(EmailOrigen);
                mail.To.Add(new MailAddress(EmailDestino));
                mail.Subject = "★★★ Preinscripcion a evento UTB";
              
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(" <img id=\"Logo.png\" alt=\"logo\" src=cid:companylogo>" + cadena_correo , UTF8Encoding.UTF8, "text/html");
                //mail.BodyEncoding = UTF8Encoding.UTF8;
                //mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                var aa = System.AppDomain.CurrentDomain.BaseDirectory;

                LinkedResource imageResource = new LinkedResource(aa + @"\img\cabecera.png");
                

                //System.Net.Mail.LinkedResource imageResource1 = new System.Net.Mail.LinkedResource(System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "/assets/img/pc1.png");


                imageResource.ContentId = "companylogo";
                imageResource.ContentType.Name = "logo.png";
                imageResource.TransferEncoding = TransferEncoding.Base64;
                imageResource.ContentType= new ContentType("image/png");

                htmlView.LinkedResources.Add(imageResource);

                mail.AlternateViews.Add(htmlView);
                //mail.Body = htmlView.ContentStream


                mail.BodyEncoding = UTF8Encoding.UTF8;
                mail.IsBodyHtml = true;
                //mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                //mail.Attachments.Add(data);
                //_log.WriteLogInfo(TypeMessageFormat.TEXT, LoggingQueue.InfoIn, "Antes de mandarlo" + mail.Body, "PC.MX.Affiliate.Monitor", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                client.Send(mail);
                //_log.WriteLogInfo(TypeMessageFormat.TEXT, LoggingQueue.InfoIn, "ya lo mando", "PC.MX.Affiliate.Monitor", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name);
                //response = true;

                mensaje_respuesta += "<p>Se le envio una copia del mensaje a su email</p>";

            }
            catch (Exception ex)
            {
                //Response.Write(ex);
                mensaje_respuesta += "<p>Ocurrio un error al enviar un mensaje a su email</p>";
            }

        }
  

        public void MensajeEstudiante(bool nuevo,double costo)
        {
            //string mensaje=""; 
            string nombre_evento = new Evento().NombreEvento(Convert.ToInt32(IdEvento.Value));

            if (nuevo)
            {
                mensaje_respuesta = "<div class='alert alert-success' role='alert'>"
                 + "<h3>Gracias por registrarse</h3>"
                 + "<p>Se le envio un correo, "
                 + " revise su bandeja de entrada</p></div>";

                cadena_correo = "<p>Saludos cordiales:</p>"
                + "<p>Un responsable del evento le llamara al número "
                + TextCelular.Value + " para confirmar su participacion</p>"
                + "<p>Esta preinscrito en el evento " + nombre_evento + "</p>";
            }
            else
            {
                //revisar si tiene costo para emitir un codigo de pago
                //sino inscribir
                //double costo = new Evento().costoEvento(Convert.ToInt32(IdEvento.Value)); 
                if (costo > 0)
                {
                    mensaje_respuesta = "<div class='alert alert-success' role='alert'>";

                    cadena_correo = "<h3>Estimado estudiante de la UTB</h3>"
                        + "<p>Esta preinscrito en el evento " + nombre_evento + "</p>"
                    + "<p>Si desea confirmar su participación al evento debe "
                    + "comunicarse con su Dirección de Carrera</p>";
                    //+"cancelar en cajas con el siguiente el codigo:" + codigo + " </p>";

                    mensaje_respuesta += cadena_correo + "</div>";
                    
                }
                else 
                {
                    mensaje_respuesta = "<div class='alert alert-success' role='alert'>";

                    cadena_correo = "<h3>Estimado estudiante de la UTB</h3>"
                    + "<p>Esta Pre-inscrito en el evento " + nombre_evento + "</p>"
                    + "<p></p>";

                    mensaje_respuesta += cadena_correo + "</div>";
                    //+ "<p>Recibira una copia de este mensaje en su email</p>";

                }
            }
        }
    }
}