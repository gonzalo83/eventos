﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;

namespace Eventos
{
    public class coneccion
    {

        public string res;
        public SqlConnection connection;
        public coneccion()
        {
            try
            {
                connection = new SqlConnection(ConfigurationManager.ConnectionStrings["campus_db"].ConnectionString);
                //Console.WriteLine("coneccion a base de datos");
                res = "coneccion base de datos";
                //Console.WriteLine("========================");

            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.ToString());
                res = ex.ToString();
                throw;
            }
        }

        public void Cerrar()
        {
            if (connection.State == System.Data.ConnectionState.Open)
            {
                connection.Close();
            }

        }
        public void Abrir()
        {
            connection.Open();
        }

        public SqlConnection Conectar()
        {
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                
            }
            return connection;
        }
    }
}
