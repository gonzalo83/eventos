﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Eventos
{
    public class Docente
    {
        public int _idDocente { get; set; }
        public string _imagen { get; set; }
        public string _grado { get; set; }
        public string _apPaterno { get; set; }
        public string _apMaterno { get; set; }
        public string _nombres { get; set; }

        public string _nombreCompleto { get; set; }
        public string _cedulaIdentidad { get; set; }
        public string _informacionDocente { get; set; }

        public Docente()
        { 
        }
        public Docente(int id, string imagen,string grado,string nombreCompleto, string informacionDocente)
        {
            _idDocente = id;
            _imagen = imagen;
            _grado = grado;
            _nombreCompleto = nombreCompleto;
            _informacionDocente = informacionDocente;
        }
        public void Datos(string id)
        {

            coneccion Conn = new coneccion();
            string consulta = "select * from evt_Docente where idDocente=@p1";

            SqlCommand command = new SqlCommand(consulta, Conn.Conectar());
            command.Parameters.AddWithValue("@p1", id);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {

                //1, "", "PhD ", "Alvaro Castro", "Ingeniero Comercial"
                //_idDocente = Convert.ToInt32(reader["idDocente"].ToString());
                _idDocente = Convert.ToInt32(id);
                _grado = reader["grado"].ToString();
                _nombreCompleto = reader["nombres"].ToString();
                _nombreCompleto += " " + reader["apPaterno"].ToString();
                _nombreCompleto += " " + reader["apMaterno"].ToString();
                _informacionDocente = reader["informacionDocente"].ToString();
                //_informacionDocente = row["contenido"].ToString();

                char delimitador = '-';
                string[] valores = _informacionDocente.Split(delimitador);

                if (_informacionDocente != "")
                {
                    _informacionDocente = "";
                    foreach (string cont in valores)
                    {
                        if (cont != "")
                        {
                            _informacionDocente += "<li>";
                           /* _informacionDocente += "<svg class=\"svg-icon\" viewBox=\"0 0 20 20\">";
                            _informacionDocente += "<path fill = \"none\" d=\"M7.197,16.963H7.195c-0.204,0-0.399-0.083-0.544-0.227l-6.039-6.082c-0.3-0.302-0.297-0.788,0.003-1.087";
                            _informacionDocente += "C0.919,9.266,1.404,9.269,1.702,9.57l5.495,5.536L18.221,4.083c0.301-0.301,0.787-0.301,1.087,0c0.301,0.3,0.301,0.787,0,1.087";
                            _informacionDocente += "L7.741,16.738C7.596,16.882,7.401,16.963,7.197,16.963z\"></path>";
                            _informacionDocente += "</svg> ";*/
                            _informacionDocente += cont + "</li>";
                        }
                    }
                    
                }
                _imagen = ConfigurationManager.AppSettings["url_admin"] + reader["imagen"].ToString();
            }
            reader.Close();
            Conn.Cerrar();
            //_nombreCompleto = "hola mundo";
            //objCon.Cerrar();
        }
    }

}