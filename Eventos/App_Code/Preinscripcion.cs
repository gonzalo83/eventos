﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

/// <summary>
/// Descripción breve de Preinscripcion
/// </summary>
namespace Eventos
{
    public class Preinscripcion
    {
        public int _idPreinscripcion { get; set; }
        public int _idEvento { get; set; }
        public DateTime _fechaPreinscripcion { get; set; }
        public string _cedulaIdentidad { get; set; }
        public string _nombres { get; set; }
        public string _apPaterno { get; set; }
        public string _apMaterno { get; set; }
        public DateTime _fechaNacimiento { get; set; }
        public string _numeroCelular { get; set; }
        public string _correo { get; set; }

        public bool _nuevo { get; set; }
        
        public Preinscripcion()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }
        public async Task<bool>  Save()
        {
            try
            {
                coneccion objCon = new coneccion();

                string campos = "idEvento,fechaPreinscripcion,cedulaIdentidad,nombres,apPaterno,"
            +"apMaterno,fechaNacimiento,numeroCelular,correo,nuevo";
            string sql = "insert into evt_Preinscripcion(" + campos + ") "
                      
            + "values (@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10)";

                objCon.Abrir();
                SqlCommand command = new SqlCommand(sql, objCon.connection);
                command.Parameters.AddWithValue("@p1", _idEvento);
                command.Parameters.AddWithValue("@p2", _fechaPreinscripcion);
                command.Parameters.AddWithValue("@p3", _cedulaIdentidad);
                command.Parameters.AddWithValue("@p4", _nombres);
                command.Parameters.AddWithValue("@p5", _apPaterno);
                command.Parameters.AddWithValue("@p6", _apMaterno);
                command.Parameters.AddWithValue("@p7", _fechaNacimiento);
                command.Parameters.AddWithValue("@p8", _numeroCelular);
                command.Parameters.AddWithValue("@p9", _correo);
                command.Parameters.AddWithValue("@p10", _nuevo.Equals(true) ? 1 : 0);
                await command.ExecuteNonQueryAsync();
                objCon.Cerrar();
            
                return true;
            }
            catch
            {
                return false;
            }
            
        }

    }
}
