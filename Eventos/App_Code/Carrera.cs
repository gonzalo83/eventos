﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Eventos
{
    public class Carrera
    {
        public int idCarrera { get; set; }
        public string nombreCarrera { get; set; }

        public Carrera()
        {

        }
        public Carrera(int idEvento)
        {
            coneccion objConn = new coneccion();
            string consulta = "select C.nombreCarrera,C.idCarrera from carrera C "
            +"Inner join evt_CarreraEvento CE "
            +"ON CE.idCarrera=C.idCarrera "
            +"inner join evt_Evento E "
            +"ON E.idEvento=CE.idEvento and CE.idEvento =@p1";

            //return consulta;
            SqlCommand command = new SqlCommand(consulta, objConn.Conectar());
            command.Parameters.AddWithValue("@p1", idEvento);
            SqlDataReader reader = command.ExecuteReader();
            nombreCarrera = "";
            while (reader.Read())
            {
                nombreCarrera += "¬"+reader["nombreCarrera"].ToString();
                //return true;
            }

        }
    }

    
}