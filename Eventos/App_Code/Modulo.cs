﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

/// <summary>
/// Descripción breve de Modulo
/// </summary>
namespace Eventos
{
    public class Modulo
    {
        public int _idModulo { get; set; }
        public int _idEvento { get; set; }
        public int _idDocente { get; set; }
        public string _nombreModulo { get; set; }
        public DateTime _fechaInicio { get; set; }
        public DateTime _fechaFin { get; set; }
        public string _fecha { get; set; }
        //public string _tipoModulo { get; set; }
        public string _enlace { get; set; }
        public bool _prioridadDocente { get; set; }
        public string _horario { get; set; }
        public string _contenido { get; set; }

        Docente _docente = null;

        //coneccion objCon = null;

        public Modulo()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }
        public Modulo(int id,int idEvento,int idDocente,bool prioridadDocente,string nombreModulo)
        {
            _idModulo = id;
            _idEvento = idEvento;
            _idDocente = idDocente;
            _nombreModulo = nombreModulo;
            _prioridadDocente = prioridadDocente;
        }
        public Modulo(DataRow row,Docente docente)
        {
            _idModulo = Convert.ToInt32(row["idModulo"].ToString());
            _idEvento = Convert.ToInt32(row["idEvento"].ToString());
            _nombreModulo = row["nombreModulo"].ToString();
            _docente = docente;
            _horario = row["horario"].ToString(); 
            _fechaInicio= Convert.ToDateTime(row["fechaInicio"].ToString());
            _fechaFin = Convert.ToDateTime(row["fechaFin"].ToString());
            if (_fechaInicio == _fechaFin)
            {
                _fecha = _fechaInicio.ToShortDateString();
            }
            else
            {
                _fecha = _fechaInicio.ToShortDateString() + " al " + _fechaFin.ToShortDateString();
            }
           // _tipoModulo = tipoModulo;
            _enlace = row["enlace"].ToString();
            _contenido= row["contenido"].ToString();

            char delimitador = '-';
            string[] valores = _contenido.Split(delimitador);

            if (_contenido != "")
            {
                _contenido = "<ul>";
                foreach (string cont in valores)
                {
                    if (cont!="")
                    {
                        _contenido += "<li>";
                        /*+ "<svg class=\"svg-icon\" viewBox=\"0 0 20 20\">"
                        + "<path fill = \"none\" d=\"M7.197,16.963H7.195c-0.204,0-0.399-0.083-0.544-0.227l-6.039-6.082c-0.3-0.302-0.297-0.788,0.003-1.087"
                        + "C0.919,9.266,1.404,9.269,1.702,9.57l5.495,5.536L18.221,4.083c0.301-0.301,0.787-0.301,1.087,0c0.301,0.3,0.301,0.787,0,1.087"
                        + "L7.741,16.738C7.596,16.882,7.401,16.963,7.197,16.963z\"></path>";
                        _contenido += "</svg> ";*/
                        string sublista = Sublista(cont);
                        _contenido += sublista+ "</li>";
                    }
                }
                _contenido += "</ul>";
            }

        }
        private string Sublista(string lista)
        {
            string Resultado="";
            char delimitador = '(';
            string[] valores = lista.Split(delimitador);
            Resultado = valores[0] + "<ul style='list-style-type: circle;padding:0.3rem;'>";
            if (lista != "")
            {
               

                for(int i=1;i< valores.Count();i++)
                {
                    delimitador = ')';
                    string[] aux = valores[i].Split(delimitador);
                    Resultado += "<li>"
                        + aux[0]+ "</li>";
                }
                
            }
            Resultado += "</ul>";
            return Resultado;
        }
        public Modulo(int id)
        {
            coneccion objConn = new coneccion();
            string consulta = "select * from evt_Modulo where prioridadDocente=1 and idEvento=@p1";

            SqlCommand command = new SqlCommand(consulta, objConn.Conectar());
            command.Parameters.AddWithValue("@p1", id);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {

                //1, "", "PhD ", "Alvaro Castro", "Ingeniero Comercial"
                _idDocente = Convert.ToInt32(reader["idDocente"].ToString());
            }
            reader.Close();
            objConn.Cerrar();
        }
        
        
        public DataTable SetAgenda(int idEvento)
        {
            List<Modulo> mod = new List<Modulo>();
            string sql = "select * from evt_Modulo where idEvento=@p1 order by fechaInicio";

            coneccion objCon = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter( );
            // Create the commands.
            da.SelectCommand = new SqlCommand(sql, objCon.Conectar());
            da.SelectCommand.Parameters.AddWithValue("@p1",idEvento);
            DataSet dst = new DataSet();
            da.Fill(dst);
            DataTable modulos = dst.Tables[0];
            objCon.Cerrar();
            //DataTable modulos = GetDataTable(sql);
            //Docente exponente = new Docente();
            foreach (DataRow row in modulos.Rows)
            {
                Docente exponente = new Docente();
                exponente.Datos(row["idDocente"].ToString());
                //Docente exponente = new Docente("5");
                mod.Add(new Modulo(row,exponente));
            }

            DataTable table = new DataTable();
            table.Columns.Add("idModulo", typeof(int));
            table.Columns.Add("idEvento", typeof(int));
            table.Columns.Add("nombreModulo", typeof(string));
            table.Columns.Add("imagenDocente", typeof(string));
            table.Columns.Add("gradoDocente", typeof(string));
            table.Columns.Add("nombreDocente", typeof(string));
            table.Columns.Add("informacionDocente", typeof(string));
            table.Columns.Add("horario", typeof(string));
            table.Columns.Add("fecha", typeof(string));
            //table.Columns.Add("tipoModulo", typeof(string));
            table.Columns.Add("enlace", typeof(string));
            table.Columns.Add("contenido", typeof(string));
            foreach (Modulo str in mod)
            {
                DataRow row = table.NewRow();
                row["idModulo"] = str._idModulo;
                row["idEvento"] = str._idEvento;
                row["nombreModulo"] = str._nombreModulo;
                row["imagenDocente"] = str._docente._imagen;
                row["gradoDocente"] = str._docente._grado;
                row["nombreDocente"] = str._docente._nombreCompleto;
                row["informacionDocente"] = str._docente._informacionDocente;
                row["horario"] = str._horario;
                row["fecha"] = str._fecha;
               // row["tipoModulo"] = str._tipoModulo;
                row["enlace"] = str._enlace;
                row["contenido"] = str._contenido;
                table.Rows.Add(row);
            }
            return table;

        }

        public DataTable SetAgendaUsuario(int idEvento)
        {
            List<Modulo> mod = new List<Modulo>();
            string sql = "select * from evt_Modulo where idEvento=@p1 order by fechaInicio";

            coneccion objCon = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter();
            // Create the commands.
            da.SelectCommand = new SqlCommand(sql, objCon.Conectar());
            da.SelectCommand.Parameters.AddWithValue("@p1", idEvento);
            DataSet dst = new DataSet();
            da.Fill(dst);
            DataTable modulos = dst.Tables[0];
            objCon.Cerrar();
            //DataTable modulos = GetDataTable(sql);
            //Docente exponente = new Docente();
            for (int i = 0; i < modulos.Rows.Count; i++)
            {
                var contenido = modulos.Rows[i]["contenido"].ToString();

                char delimitador = '-';
                string[] valores = contenido.Split(delimitador);

                if (contenido != "")
                {
                    contenido = "<ul>";
                    foreach (string cont in valores)
                    {
                        if (cont != "")
                        {
                            contenido += "<li>";

                            string sublista = Sublista(cont);
                            contenido += sublista + "</li>";
                        }
                    }
                    contenido += "</ul>";
                }

                modulos.Rows[i]["contenido"] = contenido;
            }//*/
            return modulos;

        }

    }
}