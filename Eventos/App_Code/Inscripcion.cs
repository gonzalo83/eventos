﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;


namespace Eventos
{
    public class Inscripcion
    {
        public int _idInscripcion { get; set; }
        public int _idEvento { get; set; }
        public int _idEstudiante { get; set; }
        public DateTime _fecha_Inscripcion { get; set; }
        public string _nota_final { get; set; }
        public string _CodigoPago { get; set; }
        public DateTime _fecha_registro { get; set; }
        public string _usuario_registro { get; set; }
        public DateTime _fecha_modificacion { get; set; }
        public string _usuario_modificacion { get; set; }

        public Inscripcion()
        {
        }

        public bool Save()
        {
            string campos = "idEvento,idEstudiante,fecha_Inscripcion,CodigoPago,nota_final,"
            + "a_fecha_registro,a_usuario_registro,a_fecha_modificacion,a_usuario_modificacion";
            string sql = "insert into evt_Inscripcion(" + campos + ") values ("
            //coneccion objConn = new coneccion();
            + _idEvento + ","
            + _idEstudiante + ",'"
            + _fecha_Inscripcion + "','"
            + _CodigoPago + "','"
            + _nota_final + "','"
            + _fecha_registro + "','"
            + _usuario_registro + "','"
            + _fecha_modificacion + "','"
            + _usuario_modificacion + "')";
            try
            {
                //coneccion conexion=new coneccion();
                Ejecutar(sql);
                return true;
            }
            catch
            {
                return false;
            }
            //return sql;
        }
        public void Ejecutar(string sql)
        {
            coneccion objCon = new coneccion();
            objCon.Abrir();
            SqlCommand command = new SqlCommand(sql, objCon.connection);
            //command.Connection.Open();
            command.ExecuteNonQuery();
            objCon.Cerrar();
        }
        public bool verificar(int idEstudiante, string idEvento)
        {

            coneccion objConn = new coneccion();
            string consulta = "select * from evt_Inscripcion where idEvento=@p1 "
                + " and idEstudiante=@p2";
            objConn.Abrir();
            SqlCommand command = new SqlCommand(consulta, objConn.connection);
            command.Parameters.AddWithValue("@p1", idEvento);
            command.Parameters.AddWithValue("@p2", idEstudiante);
            SqlDataReader reader = command.ExecuteReader();
            bool sw = false;
            if (reader.Read())
            {
                sw = true;
            }
            reader.Close();
            objConn.Cerrar();
            return sw;
        }
        public Inscripcion(int idEstudiante, int idEvento)
        {

            coneccion objConn = new coneccion();
            string consulta = "select * from evt_Inscripcion where idEvento=@p1"
                + " and idEstudiante=@p2";
            objConn.Abrir();
            SqlCommand command = new SqlCommand(consulta, objConn.connection);
            command.Parameters.AddWithValue("@p1", idEvento);
            command.Parameters.AddWithValue("@p2", idEstudiante);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                _idInscripcion = Int32.Parse(reader["idInscripcion"].ToString());
                _CodigoPago = reader["CodigoPago"].ToString();
                _fecha_Inscripcion = Convert.ToDateTime(reader["fecha_Inscripcion"].ToString());
            }
            reader.Close();
            objConn.Cerrar();
            //return ins;
        }
        public Inscripcion(string codigo)
        {

            coneccion objConn = new coneccion();
            string consulta = "select * from evt_Inscripcion where CodigoPago='"+ codigo+"'";
            objConn.Abrir();
            SqlCommand command = new SqlCommand(consulta, objConn.connection);

            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                _idInscripcion = Int32.Parse(reader["idInscripcion"].ToString());
                _CodigoPago = reader["CodigoPago"].ToString();
                _fecha_Inscripcion = Convert.ToDateTime(reader["fecha_Inscripcion"].ToString());
                _idEvento = Int32.Parse(reader["idEvento"].ToString());
                _idEstudiante = Int32.Parse(reader["idEstudiante"].ToString());

            }
            reader.Close();
            objConn.Cerrar();
            //return ins;
        }
    }
}
