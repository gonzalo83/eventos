﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Eventos;
using System.Configuration;

/// <summary>
/// Descripción breve de Evento
/// </summary>

namespace Eventos
{
    public class Evento
    {
        public int _idEvento { get; set; }
        public int _idCarrera { get; set; }
        public string _nombreEvento { get; set; }
        public string _tipoEvento { get; set; }
        public string _imagen { get; set; }
        public double _costo { get; set; }
        public string _modalidad { get; set; }
        public string _direccion { get; set; }
        public string _cantidadHoras { get; set; }
        public string _cupoMaximo { get; set; }
        public DateTime _fechaInicio { get; set; }
        public DateTime _fechaFin { get; set; }

        public string _fecha { get; set; }
        public string _informacionEvento { get; set; }
        public string _objetivoGeneral { get; set; }


        public Modulo _modulo = null;
        public Docente _docente = null;


        public List<Evento> _eventos;

        //coneccion objCon = null;

        public Evento()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
            _eventos = new List<Evento>();
        }


        public Evento(int id, string nombre, string imagen, string objetivo)
        {
            _idEvento = id;
            _nombreEvento = nombre;
            _imagen = ConfigurationManager.AppSettings["url_admin"] + imagen;
            //_modulo = modulo;
            //_docente = docente;
            _objetivoGeneral = objetivo;
        }
        public Evento(Evento evento, Modulo modulo, Docente docente)
        {
            _idEvento = evento._idEvento;
            _nombreEvento = evento._nombreEvento;
            _tipoEvento = evento._tipoEvento;
            //_imagen = evento._imagen;
            _imagen = evento._imagen;
            _modulo = modulo;
            _docente = docente;
            if (evento._fechaInicio == evento._fechaFin)
            {
                _fecha = evento._fechaInicio.ToShortDateString();
            }
            else
            {
                _fecha = evento._fechaInicio.ToShortDateString() + " al " + evento._fechaFin.ToShortDateString();
            }
            _costo = evento._costo;
            _modalidad = evento._modalidad;
            _informacionEvento = evento._informacionEvento;
            _direccion = evento._direccion;
            _cantidadHoras = evento._cantidadHoras;
        }

        public Evento(int id)
        {

            coneccion objConn = new coneccion();
            string consulta = "select * from evt_Evento where idEvento=@p1";

            SqlCommand command = new SqlCommand(consulta, objConn.Conectar());
            command.Parameters.AddWithValue("@p1", id);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {


                //Evento(1, "Técnicas de estudio", "img/evento1.jpg",
                _idEvento = Convert.ToInt32(reader["idEvento"].ToString());
                _nombreEvento = reader["nombreEvento"].ToString();
                _tipoEvento = reader["tipoEvento"].ToString();
                _imagen = reader["imagen"].ToString();
                //fecha,costo,modalidad
                _fechaInicio = Convert.ToDateTime(reader["fechaInicio"].ToString());
                _fechaFin = Convert.ToDateTime(reader["fechaFin"].ToString());
                if (_fechaInicio == _fechaFin)
                {
                    _fecha = _fechaInicio.ToShortDateString();
                }
                else
                {
                    _fecha = _fechaInicio.ToShortDateString() + " al " + _fechaFin.ToShortDateString();
                }
                _costo = Convert.ToDouble(reader["costo"].ToString());
                _modalidad = reader["modalidad"].ToString();
                _informacionEvento = reader["informacionEvento"].ToString();
                _direccion = reader["direccion"].ToString();
                _cantidadHoras = reader["cantidadHoras"].ToString();
            }
            reader.Close();
            objConn.Cerrar();
        }

    
        
        public DataTable SetEventos()
        {
            List<Evento> evt = new List<Evento>();

          
            string sql = "select idEvento as id,nombreEvento as nombre,imagen,objetivoGeneral as objetivo from evt_Evento order by fechaInicio Desc";
            coneccion objCon = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter();
            // Create the commands.
            da.SelectCommand = new SqlCommand(sql, objCon.Conectar());
            //da.SelectCommand.Parameters.AddWithValue("@p1", idEvento);
            DataSet dst = new DataSet();
            da.Fill(dst);
            DataTable eventos = dst.Tables[0];
            objCon.Cerrar();
            //DataTable eventos = GetDataTable(sql);

           
            int id_Evento;
            //Docente docente = new Docente();
            foreach (DataRow row in eventos.Rows)
            {
                id_Evento = Convert.ToInt32(row["id"].ToString());
               
                evt.Add(new Evento(id_Evento, row["nombre"].ToString(), row["imagen"].ToString(), row["objetivo"].ToString()));
            }

            DataTable table = new DataTable();
            table.Columns.Add("id", typeof(int));
            table.Columns.Add("nombre", typeof(string));
            table.Columns.Add("costo", typeof(double));
            table.Columns.Add("imagen", typeof(string));
            table.Columns.Add("objetivo", typeof(string));
    
            foreach (Evento str in evt)
            {
                DataRow row = table.NewRow();
                row["id"] = str._idEvento;
                row["nombre"] = str._nombreEvento;
                row["costo"] = str._costo;
                row["imagen"] = str._imagen;
                row["objetivo"] = str._objetivoGeneral;
     
                table.Rows.Add(row);
            }
            return table;

        }
        /***************para listar eventos del filtro*****************************************/
        public DataTable SetEventos(string tipoEVT)
        {
            List<Evento> evt = new List<Evento>();

            string sql = "select idEvento as id,nombreEvento as nombre,imagen,objetivoGeneral as objetivo from evt_Evento where tipoEvento=@p1";
            sql += " order by fechaInicio DESC";
            coneccion objCon = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter();
            // Create the commands.
            da.SelectCommand = new SqlCommand(sql, objCon.Conectar());
            da.SelectCommand.Parameters.AddWithValue("@p1", tipoEVT);
            DataSet dst = new DataSet();
            da.Fill(dst);
            DataTable eventos = dst.Tables[0];
            objCon.Cerrar();

            int id_Evento;
            //Docente docente = new Docente();
            foreach (DataRow row in eventos.Rows)
            {
                id_Evento = Convert.ToInt32(row["id"].ToString());
                //conseguir el id_Docente de modulo
                evt.Add(new Evento(id_Evento, row["nombre"].ToString(), row["imagen"].ToString(), row["objetivo"].ToString()));
            }

            DataTable table = new DataTable();
            table.Columns.Add("id", typeof(int));
            table.Columns.Add("nombre", typeof(string));
            table.Columns.Add("costo", typeof(double));
            table.Columns.Add("imagen", typeof(string));
            table.Columns.Add("objetivo", typeof(string));

            foreach (Evento str in evt)
            {
                DataRow row = table.NewRow();
                row["id"] = str._idEvento;
                row["nombre"] = str._nombreEvento;
                row["costo"] = str._costo;
                row["imagen"] = str._imagen;
                row["objetivo"] = str._objetivoGeneral;
                table.Rows.Add(row);
            }
            return table;

        }

        /***************para listar eventos de un usuario************************/
        public DataTable SetEventosUsuario(string _id_usuario)
        {
            //obtener lista de eventos
            string sql = "select E.idEvento as id,E.nombreEvento as nombre,E.tipoEvento as tipo from evt_Evento E";
            sql += " JOIN evt_Inscripcion P";
            sql += " ON E.idEvento = P.idEvento and P.idEstudiante=@p1";
            coneccion objCon = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter();
            // Create the commands.
            da.SelectCommand = new SqlCommand(sql, objCon.Conectar());
            da.SelectCommand.Parameters.AddWithValue("@p1", _id_usuario);
            DataSet dst = new DataSet();
            da.Fill(dst);
            DataTable eventos = dst.Tables[0];
            objCon.Cerrar();
            return eventos;

        }

        public string NombreEvento(int id)
        {

            coneccion objConn = new coneccion();
            string consulta = "select nombreEvento from evt_Evento where idEvento=@p1";

            SqlCommand command = new SqlCommand(consulta, objConn.Conectar());
            command.Parameters.AddWithValue("@p1", id);
            SqlDataReader reader = command.ExecuteReader();
            string nombre = "";
            if (reader.Read())
            {
                nombre = reader["nombreEvento"].ToString();
            }
            return nombre;
        }
        public double CostoEvento(int id)
        {

            coneccion objConn = new coneccion();
            string consulta = "select costo from evt_Evento where idEvento=@p1";

            SqlCommand command = new SqlCommand(consulta, objConn.Conectar());
            command.Parameters.AddWithValue("@p1", id);
            SqlDataReader reader = command.ExecuteReader();
            double costo = 0;
            if (reader.Read())
            {
                costo = Convert.ToDouble(reader["costo"].ToString());
            }
            return costo;
        }
    }
}

