﻿<%@ Page Title=""  Async="true" Language="C#" MasterPageFile="~/Eventos.Master" AutoEventWireup="true" CodeBehind="Pre-inscripcion.aspx.cs" Inherits="Eventos.Pre_inscripcion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        .user-input-wrp {
            position: relative;
            width: 50%;
        }

            .user-input-wrp .inputText {
                width: 100%;
                outline: none;
                border: none;
                border-bottom: 1px solid #777;
                box-shadow: none !important;
            }

                .user-input-wrp .inputText:focus {
                    border-color: blue;
                    border-width: medium medium 2px;
                }

            .user-input-wrp .floating-label {
                position: absolute;
                pointer-events: none;
                top: 5px;
                left: 5px;
                font-size: 13px;
                transition: 0.2s ease all;
            }

            .user-input-wrp .inputText:focus ~ .floating-label,
            .user-input-wrp .inputText:not(:focus):valid ~ .floating-label {
                top: 0px;
                left: 5px;
                font-size: 15px;
                opacity: 1;
            }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>

    <div class="container">
        <asp:Panel ID="Panel1" runat="server">
            <div class="container">
                <div class="row">

                    <div class="col-xl-7 col-sm-10 texto-informativo">
                        <h3>Pre-inscripción</h3>
                        <asp:HiddenField ID="IdEvento" runat="server" />
                        <asp:CheckBox ID="CheckNuevo" runat="server" Visible="false" />
                        <div class="row">
                            <div class="col-xl-2 col-sm-3">
                                <img src="iconos/inscribir.jpg" />
                            </div>
                            <div class="col-xl-10 col-sm-9">
                                <hr />
                                <asp:Label ID="Lbl_InformacionEvento" runat="server" Text=""></asp:Label>
                                <hr />
                            </div>
                        </div>

                        <div class="user-input-wrp">
                            <br />
                            <input class="inputText" required id="TextCI" runat="server" />
                            <span class="floating-label">Cedula identidad</span>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="La cedula es obligatoria"
                                ControlToValidate="TextCI" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>


                        <div class="form-group">
                            <asp:Button ID="Btn_Verificar" runat="server" Text="Verificar" OnClick="Btn_Verificar_Click" CssClass="btn btn-dark" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="Panel2" runat="server">
            <div class="container">
                <div class="row">

                    <div class="col-xl-7 col-sm-10  texto-informativo">

                        <p>
                            <asp:Label ID="Lbl_VerificarCI" runat="server" Text=""></asp:Label></p>

                        <div class="user-input-wrp">
                            <br />
                            <input class="inputText" required id="TextEmail" runat="server" />
                            <span class="floating-label">Email</span>
                            <asp:RequiredFieldValidator ID="ValidatorEmail" runat="server" ErrorMessage="Su email es obligatorio"
                                ControlToValidate="TextEmail" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:Label ID="LabelErrorEmail" runat="server" Text="" AssociatedControlID="TextEmail"></asp:Label>
                        </div>

                        <div class="user-input-wrp">
                            <br />
                            <input class="inputText" required id="TextCelular" runat="server" />
                            <span class="floating-label">Celular</span>
                            <asp:RequiredFieldValidator ID="ValidatorCelular" runat="server" ErrorMessage="Su número de celular es obligatorio"
                                ControlToValidate="TextCelular" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>

                        <div class="user-input-wrp">
                            <br />
                            <input class="inputText" required id="TextNombre" runat="server" />
                            <span class="floating-label">Nombre</span>
                            <asp:RequiredFieldValidator ID="ValidatorNombre" runat="server" ErrorMessage="Escriba su nombre"
                                ControlToValidate="TextNombre" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>

                        <div class="user-input-wrp">
                            <br />
                            <input class="inputText" required id="TextPaterno" runat="server" />
                            <span class="floating-label">Ap. Paterno</span>
                            <asp:RequiredFieldValidator ID="ValidatorPaterno" runat="server" ErrorMessage="Escriba su apellido paterno"
                                ControlToValidate="TextPaterno" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>

                        <div class="user-input-wrp">
                            <br />
                            <input class="inputText" required id="TextMaterno" runat="server" />
                            <span class="floating-label">Ap. Materno</span>
                            <asp:RequiredFieldValidator ID="ValidatorMaterno" runat="server" ErrorMessage="Escriba su apellido materno"
                                ControlToValidate="TextMaterno" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>

                        <div class="user-input-wrp">
                            <br />
                            <br />
                            <input class="inputText" required id="TextNacimiento" runat="server" />
                            <span class="floating-label">Fecha de Nacimiento<br /> (Dia/Mes/Año) Ej: 31/1/2000</span>
                            <asp:RequiredFieldValidator ID="ValidatorNacimiento" runat="server" ErrorMessage="Su fecha de nacimiento es necesario"
                                ControlToValidate="TextNacimiento" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server"
                                ControlToValidate="TextNacimiento" ErrorMessage="Formato de fecha inválido"
                                Type="Date" Operator="DataTypeCheck" ForeColor="Red" SetFocusOnError="true"></asp:CompareValidator>
                        </div>

                        <div class="form-group">
                            <asp:Button ID="Btn_Registrar" runat="server" OnClick="Btn_Registrar_Click" Text="Registrar" CssClass="btn btn-dark" />
                        </div>
                        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                        <ProgressTemplate>
                            <div style="background-color:#f5b718;">
                          Enviando datos al servidor...
                            </div>
                        </ProgressTemplate>
                        </asp:UpdateProgress>
                    </div>
                </div>
            </div>
        </asp:Panel>




        <asp:Panel ID="Panel3" runat="server">
            <asp:Label ID="lblmensajefinal" runat="server" Text=""></asp:Label>
            <asp:Literal ID="Ltl_Miembro" runat="server"></asp:Literal>
        </asp:Panel>
    </div>
            </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

