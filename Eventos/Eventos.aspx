﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Eventos.Master" AutoEventWireup="true" CodeBehind="Eventos.aspx.cs" Inherits="Eventos.Eventos1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>

.card img{
    
    width:100%;
    height:12rem;
     display:inline;
}

.card p{
     color:black;
}
.mostrarArriba{
    animation: mostrarArriba 1.5s;
}
    
@keyframes mostrarArriba {
    0%{
        transform:translateY(60px);
    }
    100%{
        transform:translateY(0);
    }
}

.card .slide.slide1 {

    position: relative;

    display: block;

    /*justify-content: center;*/

    align-items: center;

    z-index: 1;

    transition: .7s;

    /*transform: translateY(10px);*/
    transform: translateY(0px);

}

.card:hover .slide.slide1{

    transform: translateY(0px);

}

.card:hover img{

   display:none;
}
.card:hover span{

   display:none;
}



.card .slide.slide2 {

    position: relative;

    display: none;

    justify-content: center;

    align-items: center;

    /*padding: 20px;*/

   /* box-sizing: border-box;*/

    transition: .8s;

    /*transform: translateY(-100px);*/
    transform: translateY(-10px);

    /*box-shadow: 0 20px 40px rgba(0,0,0,0.4);*/

}

.card:hover .slide.slide2{

    transform: translateY(0);
    display:block;
}



</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
    <br />
  <div class="container">
  
    <h2>Catálogo de Eventos</h2>
   <p>
    En este catálogo puedes encontrar una amplia selección de eventos que ofrece la universidad, si requieres información adicional o tienes interés en un área específica puedes contactar al Call Center 2-2180808.</p>
      <p>
    <asp:DropDownList ID="DropDownList2" runat="server" 
            CssClass="btn btn-secondary dropdown-toggle" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
            <asp:ListItem Value="TALLER">TALLERES</asp:ListItem>
            <asp:ListItem Value="SEMINARIO">SEMINARIOS</asp:ListItem>
            <asp:ListItem Value="VIAJE">VIAJES</asp:ListItem>
            <asp:ListItem Value="CURSO">CURSOS</asp:ListItem>
            <asp:ListItem>OTROS</asp:ListItem>
        </asp:DropDownList></p>   

      <asp:ListView ID="ListView1" runat="server" GroupPlaceholderID="groupPlaceHolder1"
            ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="OnPagePropertiesChanging">
          <EmptyDataTemplate>
         <table >
         <tr>
         <td><span style="color:red;">No existen datos que mostrar</span> </td>
         </tr>
         </table>
         </EmptyDataTemplate>
          <EmptyItemTemplate>
         <td><td/>
         </EmptyItemTemplate>
            <LayoutTemplate>
            
            <div class="card-columns texto-final" style="column-count:auto;display:inline-block;">
                <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
            </div>
                <div style="justify-content:center; padding-left:2rem;">
                <asp:DataPager ID="DataPager1" runat="server" PagedControlID="ListView1" PageSize="15">
                    <Fields>
                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                            ShowNextPageButton="false" />
                        <asp:NumericPagerField ButtonType="Link" />
                        <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton = "false" />
                    </Fields>
                </asp:DataPager>
                </div>
            </LayoutTemplate>
            <GroupTemplate>
            
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
  
            </GroupTemplate>
            <ItemTemplate>
                 <div class="card" style="width:15rem;height:16rem;vertical-align:top;">
                     <div class="slide slide1">
                    <img loading="lazy" src="<%# Eval("imagen") %>">
                    <br /><span><strong><%# Eval("nombre") %></strong></span>
                     </div>

                     <div class="slide slide2">
                         <a href="EventoDetalle.aspx?id=<%# Eval("id") %>" >
                    
                  <p> <%# Eval("objetivo") %></p>
                    
                   </a>
                </div>
                 
                 
                 
                 </div>
            </ItemTemplate>
            
        </asp:ListView> 
 
  </div>
 </div>



<script>

//ControlarTExt();

    function ControlarTExt() {
        var textos=document.querySelectorAll("#texto");
        const max=50;
        textos.forEach(element => {
            if(element.innerHTML.length>max){
                var nuevo=element.innerHTML.substring(0,max);
                element.innerHTML=nuevo+"....";
            }
            
        });
    }

    /* let animado=document.querySelectorAll(".card");;
    function mostrarScroll(){
        let scrollTop=document.documentElement.scrollTop;
        console.log(scrollTop);
 
        for(var i=0;i<animado.length;i++){
            let alturaAni=animado[i].offsetTop;
            if(alturaAni-200<scrollTop){
                animado[i].style.opacity=1;
                animado[i].classList.add("mostrarArriba");
            }
           /* if(scrollTop<=100||scrollTop==0){
                animado[i].style.opacity=0;
                animado[i].classList.remove("mostrarArriba");
        }
           ///
        }
    }
    window.addEventListener('scroll',mostrarScroll);*/
</script>

</asp:Content>
