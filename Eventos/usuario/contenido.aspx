﻿<%@ Page Title="" Language="C#" MasterPageFile="~/usuario/Usuario.Master" AutoEventWireup="true" CodeBehind="contenido.aspx.cs" Inherits="Eventos.usuario.contenido" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:Label ID="Lbl_vacio" runat="server" Text=""></asp:Label>
    
    <div class="container" id="evento_detalle" runat="server">
        
    <div class="texto-introduccion">
    <h5><asp:Label ID="Lbl_nombreEvento" runat="server" Text=""></asp:Label></h5>
    <img src="../iconos/reloj_icono.png"  style="width:30px;"/>16 Horas acad.
    </div>

    <div class="texto-final">
    <p>Modalidad: <asp:Label ID="Lbl_modalidad" runat="server" Text=""></asp:Label></p>
    <p>Dirección: <asp:Label ID="Lbl_direccion" runat="server" Text=""></asp:Label></p>
    <p>Fecha: <asp:Label ID="Lbl_fecha" runat="server" Text="Label"></asp:Label></p>
        <asp:Button ID="Btn_mostrar" runat="server" Text="Ver modulos" OnClick="Btn_mostrar_Click" />               
        <asp:Button ID="Btn_ocultar" runat="server" Text="Ocultar modulos" OnClick="Btn_ocultar_Click" Visible="False" />               
        <br />
        <br />
        
         <asp:Repeater ID="RepterDetails" runat="server">  
             <ItemTemplate> 
              <div>
                   <div style="padding:0.5rem 2rem;float:left;">
                    <p style="color:#f5b718;">  <%# Eval("fechaInicio") %> </p>
                     <h3>  <%# Eval("horario") %> </h3>
                    </div>
                

                    <div style="padding:0.5rem 0 0 2rem;float:left;border-left:1px solid 	#f5b718;">
                        <strong style="color:#f5b718;"><%# Eval("nombreModulo") %></strong>
                     <br />
                         
                    <div class="">
                    <%# Eval("contenido") %>
                    </div>
                     </div>
                 <div style="clear: both;"></div>

                </div>
                 </ItemTemplate>
            </asp:Repeater>
</div>

    </div>
</asp:Content>
