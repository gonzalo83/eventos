﻿<%@ Page Title="" Language="C#" MasterPageFile="~/usuario/Usuario.Master" AutoEventWireup="true" CodeBehind="usuario.aspx.cs" Inherits="Eventos.usuario.usuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style>
         .img-fluid{
        width: 348px;
        height: 348px;
      }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="pt-5 pb-5">
  <div class="container">
    <div class="row">
        <div class="col-6">
            <h3 class="mb-3">Eventos Nuevos </h3>
        </div>
        <div class="col-6 text-right">
            <a class="btn btn-primary mb-3 mr-1" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                <i class="fa fa-arrow-left">anterior</i>
            </a>
            <a class="btn btn-primary mb-3 " href="#carouselExampleIndicators2" role="button" data-slide="next">
                <i class="fa fa-arrow-right">siguiente</i>
            </a>
        </div>
      



        <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">

            <div class="carousel-inner">
                    
                <div class="carousel-item active">
                    <div class="row">

                        
                        <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>

                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">

                         <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
                           

                    </div>
                </div>
                    
            

            </div>
            
        </div>   
            


             



</section>
</asp:Content>
