﻿<%@ Page Title="" Language="C#" MasterPageFile="~/usuario/Usuario.Master" AutoEventWireup="true" CodeBehind="eventos.aspx.cs" Inherits="Eventos.usuario.eventos" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h3>MIS EVENTOS&nbsp;</h3>

 <asp:ListView ID="ListView1" runat="server" GroupPlaceholderID="groupPlaceHolder1"
            ItemPlaceholderID="itemPlaceHolder1">
            <LayoutTemplate>
            
            <div class="card-columns texto-final" style="column-count:auto;">
                <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
            </div>
            </div>
            </LayoutTemplate>
            <GroupTemplate>
                
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                
            </GroupTemplate>
            <ItemTemplate>
                 
                <div class="card border-warning mb-3" style="max-width: 18rem;">
                  <div class="card-header bg-transparent border-warning"><%# Eval("tipo") %></div>
                  <div class="card-body text-dark">
                    <h5 class="card-title"><%# Eval("nombre") %></h5>
                  </div>
                  <div class="card-footer bg-transparent border-warning">
                      <asp:Button ID="cont" runat="server" Text="Contenido" CommandArgument='<%# Eval("id") %>' OnClick="cont_Click" CssClass="btn btn-dark"/></asp:Button>
                    </div>
 
                </div>
            </ItemTemplate>
            
        </asp:ListView> 


</asp:Content>
