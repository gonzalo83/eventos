﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eventos.usuario
{
    public partial class eventos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id_usuario =(String) Session["Estudiante"];
            DataTable testDt = new Evento().SetEventosUsuario(id_usuario);
            ListView1.DataSource = testDt;
            ListView1.DataBind();
        }
        protected void cont_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string entrada = btn.CommandArgument.ToString();
            //Response.Write(entrada);
            Response.Redirect("contenido.aspx?id="+entrada);
        }
    }
}