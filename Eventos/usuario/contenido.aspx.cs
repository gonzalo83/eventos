﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Eventos;

namespace Eventos.usuario
{
    public partial class contenido : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // var id_Evento = Convert.ToInt32(Request["id"]);
            var eventoId = Request["id"];
            int id_Evento;
            if (!String.IsNullOrEmpty(eventoId) && int.TryParse(eventoId, out id_Evento))
            {
                
                
                Evento evt = new Evento(id_Evento);
                int id_est = Convert.ToInt32(Session["Estudiante"]);
                bool bandera= new Inscripcion().verificar(id_est, id_Evento.ToString());
                if (!String.IsNullOrEmpty(evt._nombreEvento) && bandera)
                {
                    Lbl_nombreEvento.Text = evt._nombreEvento;
                    //Lbl_tipoEvento.Text = evento._tipoEvento;
                    //Img_evento.ImageUrl = "http://" + evento._imagen;
                    Lbl_fecha.Text = evt._fecha;
                    //Lbl_costo.Text = evento._costo.ToString();
                    Lbl_modalidad.Text = evt._modalidad;
                    //Lbl_cantidadHoras.Text = evento._cantidadHoras;
                    Lbl_direccion.Text = evt._direccion;
                    // Lbl_informacionEvento.Text = evento._informacionEvento;
                    //Img_docente.ImageUrl = evento._docente._imagen;
                }
                else
                {
                    evento_detalle.Attributes.Add("style", "display:none;");
                    Lbl_vacio.Text = "No existe el evento o no esta inscrito";
                    Lbl_vacio.ForeColor = Color.Red;
                }

            }
            else
            {
                //Debug.Fail("ERROR : No podemos mostrar un evento sin ID");
                //throw new Exception("ERROR : no podemos cargar EventoDetalle ");
                Response.Redirect("eventos.aspx");
            }
        }

        protected void Btn_mostrar_Click(object sender, EventArgs e)
        {
            Btn_ocultar.Visible = true;
            Btn_mostrar.Visible = false;
            RepterDetails.Visible = true;
            DataTable Dt = new Modulo().SetAgendaUsuario(Convert.ToInt32(Request["id"]));
            RepterDetails.DataSource = Dt;
            RepterDetails.DataBind();
        }

        protected void Btn_ocultar_Click(object sender, EventArgs e)
        {

            Btn_ocultar.Visible = false;
            Btn_mostrar.Visible = true;
            RepterDetails.Visible = false;
        }
    }
}