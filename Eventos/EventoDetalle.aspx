﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Eventos.Master" AutoEventWireup="true" CodeBehind="EventoDetalle.aspx.cs" Inherits="Eventos.EventoDetalle" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Label ID="Lbl_vacio" runat="server" Text=""></asp:Label>

    <div class="container" id="evento_detalle" runat="server">

        
  
   <div class="row">
       <div class="col">
           
      <br />
      <asp:HiddenField ID="HiddenId" runat="server" />

       </div>

   </div>
   <div class="row" style="background-color:#f6f6ee;padding-top:1rem;">
  

     <div class="col-xl-5 col-md-7 col-sm-6">
      
         
      <asp:Image loading="lazy" ID="Img_evento" runat="server"  class="shadow-lg p-3 mb-5 bg-white rounded" style="width:400px;"/>
      

    </div>
    
   <div class="col-xl-7 col-md-5 col-sm-6">
       <h3><asp:Label ID="Lbl_nombreEvento" runat="server" Text=""></asp:Label></h3>
       <p><span style="color:olivedrab;">Tipo de evento:</span> <asp:Label ID="Lbl_tipoEvento" runat="server" Text=""></asp:Label></p>
       <p><span style="color:olivedrab;">Carrera Organizadora:</span> <asp:Label ID="Lbl_carrera" runat="server" Text=""></asp:Label></p>
       <p><span style="color:olivedrab;">Dirección:</span> <asp:Label ID="Lbl_direccion" runat="server" Text=""></asp:Label></p>
       <p><span style="color:olivedrab;">Modalidad:</span> <asp:Label ID="Lbl_modalidad" runat="server" Text=""></asp:Label></p>
       <p><span style="color:olivedrab;">Fecha:</span> <asp:Label ID="Lbl_fecha" runat="server" Text="" style="color:crimson;"></asp:Label></p>
       <p><span style="color:olivedrab;">Duración:</span> <asp:Label ID="Lbl_cantidadHoras" runat="server" Text=""></asp:Label> Hrs.</p>
       <p><span style="color:olivedrab;">Inversión: </span> <asp:Label ID="Lbl_costo" runat="server" Text=""></asp:Label></p>
       <p><span style="color:olivedrab;">Información:</span> <asp:Label ID="Lbl_informacionEvento" runat="server" Text=""></asp:Label>
                  <br /><br />
      <asp:Button ID="Button1" runat="server" Text="Preinscripcion al Evento" 
          onclick="Button1_Click" CssClass="btn btn-dark"/>
    <p><asp:Label ID="Lbl_VerificarGlobal" runat="server" Text=""></asp:Label></p>
        </div>
   
  </div>
        

  <div class="row" style="padding-top:1rem;">
    <div class="col-xl-10 col-sm-10">
        
         <h4 style="color:olivedrab;">AGENDA DEL EVENTO:  
         <asp:Label ID="Lbl_Agenda" runat="server" Text=""></asp:Label></h4>
    </div>
    </div>
     

   <asp:ListView ID="ListView1" runat="server" GroupPlaceholderID="groupPlaceHolder1"
            ItemPlaceholderID="itemPlaceHolder1">
            <LayoutTemplate>
                <div style="">
                <div class="card-columns texto-final" style="column-count:3;display:flex;">    
                <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                </div>
                </div>
                

            </LayoutTemplate>
            <GroupTemplate>
                
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                
            </GroupTemplate>
            <ItemTemplate>
                <div class="card">
                <div class="texto-modulo">
                    <div style="background-color:#f6f6ee;padding:0.5rem;color:crimson;">
                    <p>  <%# Eval("fecha") %> </p>
                    </div>
                 <h5>  <%# Eval("horario") %> </h5>
                    <p style="padding:0.5rem;color:crimson">
                        <strong><%# Eval("nombreModulo") %></strong></pstyle="background-color:#f6f6ee;padding:0.5rem;color:crimson;>
                    
                    
                    <p style="background-color:#f6f6ee;padding:0.5rem;color:black">Lo que aprenderás</p>
                    
                    <div style="padding-left:0rem;"> 
                   <%# Eval("contenido") %>
                    </div>
                    

                    <div style="background-color:#f6f6ee;padding:0.5rem;color:black;">
                        <p>Encargado</p>
                    </div>
                    <br />
                    <div style="padding-left:0.5rem;"> 
                    <img loading="lazy" class="shadow-sm p-2 mb-3" src="<%# Eval("imagenDocente") %>" style="height:8rem;width:8rem;">
                    <br />
                    <strong><%# Eval("gradoDocente") %> <%# Eval("nombreDocente") %> </strong>
                        </div>
                    <br/>
                    <ul><%# Eval("informacionDocente") %></ul>
                    

                </div>
                    </div>
            </ItemTemplate>
            
        </asp:ListView> 


</div>
  
        <script>
            $(document).ready(function () {
              
                var divs = $("div");
                //podríamos haber escrito: var elem1 = jQuery("#elem1");
                //divs.css("font-size", "32pt");
            });

        </script>


 
</asp:Content>
