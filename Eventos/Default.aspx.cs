﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eventos
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            coneccion objConn = new coneccion();
            string consulta;
            SqlCommand command;
            SqlDataReader reader;

            consulta = "select top 6 fechaInicio as fecha,nombreEvento as nombre,imagen from evt_Evento order by fechaInicio Desc";
            command = new SqlCommand(consulta, objConn.Conectar());
            reader = command.ExecuteReader();


            for (int i = 1; i <= 3 && reader.Read(); i++)
            {


                StringBuilder html = new StringBuilder();
                html.Append("<div class=\"col-md-4 mb-3 style=\"float:right;\"\">");
                html.Append("<div class=\"card\">");
                html.Append("<img loading=\"lazy\" class=\"img-fluid\" alt=\"" + reader["nombre"].ToString() + "\" src =\""+ System.Configuration.ConfigurationManager.AppSettings["url_admin"] + reader["imagen"].ToString() + "\" > ");
                html.Append("<div class=\"card-body\">");
                html.Append("<h4 class=\"card-title\">" + reader["nombre"].ToString() + "</h4>");
                html.Append("<p class=\"card-text\">Fecha de Inicio: " + Convert.ToDateTime(reader["fecha"].ToString()).ToShortDateString() + "</p>");
                html.Append("</div>");
                html.Append("</div>");
                html.Append("</div>");
                PlaceHolder1.Controls.Add(new Literal { Text = html.ToString() });
            }

            for (int i = 1; i <= 3 && reader.Read(); i++)
            {
                StringBuilder html = new StringBuilder();
                html.Append("<div class=\"col-md-4 mb-3 style=\"float:right;\"\">");
                html.Append("<div class=\"card\">");
                html.Append("<img loading=\"lazy\" class=\"img-fluid\" alt=\"" + reader["nombre"].ToString() + "\" src =\"" + ConfigurationManager.AppSettings["url_admin"] + reader["imagen"].ToString() + "\" > ");
                html.Append("<div class=\"card-body\">");
                html.Append("<h4 class=\"card-title\">" + reader["nombre"].ToString() + "</h4>");
                html.Append("<p class=\"card-text\">Fecha de Inicio: " + Convert.ToDateTime(reader["fecha"].ToString()).ToShortDateString() + "</p>");
                html.Append("</div>");
                html.Append("</div>");
                html.Append("</div>");
                PlaceHolder2.Controls.Add(new Literal { Text = html.ToString() });
            }

            reader.Close();
            objConn.Cerrar();
        }
    }
}