﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Eventos;

namespace Eventos
{
    public partial class Eventos1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                DataTable testDt = new Evento().SetEventos();
                ListView1.DataSource = testDt;
                ListView1.DataBind();
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Response.Write(DropDownList2.SelectedItem.Text);
            DataTable testDt = new Evento().SetEventos(DropDownList2.SelectedItem.Value);
            ListView1.DataSource = testDt;
            ListView1.DataBind();
        }

        protected void OnPagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            (ListView1.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            DataTable testDt = new Evento().SetEventos();
            ListView1.DataSource = testDt;
            ListView1.DataBind();
        }

    }
}