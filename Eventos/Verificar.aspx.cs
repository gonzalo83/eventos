﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Eventos
{
    public partial class Verificar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           string codigo = Request.QueryString["codigo"];
           
            Inscripcion ins = new Inscripcion(codigo);
            Estudiante student = new Estudiante(ins._idEstudiante);

            //datos de la inscripcion
            //Lbl_nroInscripcion.Text = ins._idInscripcion.ToString("D10");
            Lbl_codigo.Text = ins._CodigoPago;
            LblFechaInscripcion.Text = ins._fecha_Inscripcion.ToShortDateString();

            //datos del Estudiante
            LblStudent.Text = student._nombreCompleto;
            LblCI.Text = student._cedulaIdentidad;

            //datos del evento
            Evento evento = new Evento(ins._idEvento);
            LblEvent.Text = evento._nombreEvento;
            LblFecha.Text = evento._fecha;
            LblModalidad.Text = evento._modalidad;
            if (evento._costo > 0)
                LblCosto.Text = String.Format(" {0:c}", evento._costo); //.ToString();
            else
                LblCosto.Text = "Gratuito";

        }
    }
}