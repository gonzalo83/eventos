﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Drawing;

public partial class Preinscritos : System.Web.UI.Page
{
    Estudiante estudiante;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var eventoId = Request["id"];
        int id_Evento;
        if (!String.IsNullOrEmpty(eventoId) && int.TryParse(eventoId, out id_Evento))
        {
            //evt_Preinscripcion.SelectCommand = "SELECT * FROM [evt_Preinscripcion] where idEvento="+Request.QueryString["id"];
            idEvento.Text = id_Evento.ToString();
            Lbl_titulo.Text += Request.QueryString["evento"];


            LinkOpciones.NavigateUrl = String.Format("./Evento.aspx?id={0}", idEvento.Text);

            Response.Write(Session["rol"]);
            if ((Session["rol"] as string) != "Marketing")
            {
                // Enter correct column index.
                GridView1.Columns[0].Visible = true;
            }
            else
            {
                GridView1.Columns[0].Visible = false;
            }
            if ((Session["rol"] as string) != "administrador")
            {
                VerificaFecha();
                verificar_si_hay_cupo(idEvento.Text);
            }
        }
        else 
        {
            Response.Redirect("Index.aspx");
        }
    }


    public void GridView1_RowCommand(Object sender, GridViewCommandEventArgs e)
    {
        // If multiple buttons are used in a GridView control, use the
        // CommandName property to determine which button was clicked.
        if (e.CommandName == "Evento")
        {
            int index = Convert.ToInt32(e.CommandArgument);
           // int index = GridView1.SelectedIndex;
            GridViewRow row = GridView1.Rows[index];
            string ci = row.Cells[4].Text; 
            string nombre = row.Cells[5].Text;
            //Response.Write(".............."+nombre);
            estudiante = new Estudiante(ci);
            //string idEvento = Request["id"];//id evento
            if (estudiante._idEstudiante > 0)
            {
 

                int resp = new Inscripcion().verificar(estudiante._idEstudiante, idEvento.Text);
                if (resp == 0)
                {
                    string idIns = Inscribir();
                    var url = String.Format("Comprobante.aspx?event={0}&student={1}&ins={2}", idEvento, estudiante._idEstudiante, idIns);
                    Response.Redirect(url);
                }
                if (resp == 1)
                {
                    Lbl_mensaje.Text = "El estudiante ya esta inscrito.";
                    Lbl_mensaje.ForeColor = Color.Red;
                }
                if (resp == 2)
                {
                    Lbl_mensaje.Text = "No existe cupo.";
                    Lbl_mensaje.ForeColor = Color.Red;
                }
                    
            }
            else
            {
                Lbl_mensaje.Text = "La persona " + nombre + " no tiene creado un Id";
                Lbl_mensaje.ForeColor = Color.Red;
                //Response.Write();
            }
            
        }
    }

    public string Inscribir() 
    {
        Inscripcion inscripcion = new Inscripcion();
        inscripcion._idEvento = Int32.Parse(idEvento.Text);
        inscripcion._idEstudiante = estudiante._idEstudiante;//_idEstudiante como id de preinscripcion
        inscripcion._fecha_Inscripcion = DateTime.Now;
        inscripcion._nota_final = "";
        Random objRandom = new Random();
        string codigo = string.Format("{0:X}", objRandom.Next(1000000, 9999999));
        codigo+=string.Format("{0:X}", objRandom.Next(1000000, 9999999));
        inscripcion._CodigoPago = codigo;
        inscripcion._fecha_registro = inscripcion._fecha_Inscripcion;
        inscripcion._usuario_registro = "utb";
        inscripcion._fecha_modificacion = inscripcion._fecha_Inscripcion;
        inscripcion._usuario_modificacion = "utb";//+ _usuario_modificacion + "')";
        string mensaje = "";
        if (!inscripcion.Save())
        {
            mensaje = "Ocurrio un error al guardar los datos";
        }
        else
        {
            mensaje = inscripcion.idInscripcion(inscripcion._idEstudiante, idEvento.Text);
        }

        return mensaje;
    }

    private void VerificaFecha()
    {
        try
        {
            coneccion conex = new coneccion();
            string query1 = "select TOP 1 [fechaInicio],horario from [dbo].[evt_Modulo] where idEvento=" + Request.QueryString["id"] + " order by [fechaInicio]";
            SqlCommand command1 = new SqlCommand(query1, conex.connection);
            conex.Abrir();
            using (SqlDataReader reader = command1.ExecuteReader())
            {
                reader.Read();

                var date = DateTime.Now;
                string datetime = DateTime.Now.ToString("hh:mm:ss tt");

                string[] subs = reader.GetString(1).Split('a');
                string[] hora = subs[0].Split(':');
                DateTime ejemploFecha = new DateTime(reader.GetDateTime(0).Year, reader.GetDateTime(0).Month, reader.GetDateTime(0).Day, Int32.Parse(hora[0]), Int32.Parse(hora[1]), 0);
                bool a = false;
                if (date > ejemploFecha)
                {
                    a = true;
                }
                if (a)
                {
                    GridView1.Columns[0].Visible = false;
                    Lbl_VerificarGlobal.Text = "La fecha Actual es mayor a la fecha de Inicio del primer modulo del Evento"
                        + "<br> No se puede inscribir<br>";
                    Lbl_VerificarGlobal.ForeColor = Color.Red;
                }


            }
        }
        catch (Exception ex) { Response.Write(ex.Message); }

    }

    protected void verificar_si_hay_cupo(string idEvento)
    {
        //int resp = 0;
        coneccion objConn = new coneccion();
        objConn.Abrir();
        var sql_contar = "select cupoMaximo from evt_Evento where idEvento=" + idEvento;
        var cmd = new SqlCommand(sql_contar, objConn.connection);
        var cupo_evento = (Int32)cmd.ExecuteScalar();


        if (cupo_evento > 0)
        {
            sql_contar = "select COUNT(idInscripcion) from evt_Inscripcion where idEvento=" + idEvento;
            cmd = new SqlCommand(sql_contar, objConn.connection);
            var numero_inscritos = (Int32)cmd.ExecuteScalar();
            if (numero_inscritos >= cupo_evento)
            {
                GridView1.Columns[0].Visible = false;
                Lbl_VerificarGlobal.Text += "No existe cupo"
                    + "<br> No se puede inscribir<br>";
                Lbl_VerificarGlobal.ForeColor = Color.Red;
            }
        }

        objConn.Cerrar();

        //return resp;
    }
   

   }
