﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;
using System.Collections.Generic;

public partial class AddDocente : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            if (Request.QueryString["id"] !=null)
            {
                
                fileImg.Attributes.Remove("required");
                coneccion con = new coneccion();
                string query = "select *from [evt_Docente] where idDocente=" + Request.QueryString["id"];
                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    reader.Read();
                    txtGrado.Text = reader.GetString(2);
                    txtPaterno.Text = reader.GetString(3);
                    txtMaterno.Text = reader.GetString(4);
                    txtNombre.Text = reader.GetString(5);
                    txtCi.Text = reader.GetString(6);                    
                    txtInfo.Text = reader.GetString(8);
                    chkD.Checked = reader.GetBoolean(7);
                }
                con.Cerrar();
                botonModificar();
            }
            else { botonAgregar(); }
        }

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if(verificaCi(txtCi.Text)){
            
            Response.Redirect("Docente.aspx?id=N");
        }
        int activo=0;
        if (chkD.Checked) {
            activo = 1;
        }
       

        string UploadDir = Server.MapPath("~/img");
        string strFileName = Path.Combine(UploadDir, this.fileImg.PostedFile.FileName);
        fileImg.PostedFile.SaveAs(strFileName);
        Response.Write(strFileName);
        coneccion con = new coneccion();
        string query = "insert into[dbo].[evt_Docente]([imagen],[grado],[apPaterno],[apMaterno],[nombres],[cedulaIdentidad],estado,[informacionDocente],[a_fecha_registro],[a_usuario_registro],[a_fecha_modificacion],[a_usuario_modificacion]) select '" + "img/" + fileImg.PostedFile.FileName + "','" + txtGrado.Text + "','" + txtPaterno.Text + "','" + txtMaterno.Text + "','" + txtNombre.Text + "','" + txtCi.Text + "','"+activo+"','" + this.txtInfo.Text + "',GETDATE(),'jose',GETDATE(),'jose123'";
        SqlCommand command = new SqlCommand(query, con.connection);
        con.Abrir();
        command.ExecuteReader();        
        con.Cerrar();
        Response.Redirect("Docente.aspx");
    }
    protected void btnUpdt_Click(object sender, EventArgs e)
    {
      
        int activo = 0;
        if (chkD.Checked)
        {
            activo = 1;
        }
        if (fileImg.HasFile)
        {
            string UploadDir = Server.MapPath("~/img");
            string strFileName = Path.Combine(UploadDir, this.fileImg.PostedFile.FileName);
            fileImg.PostedFile.SaveAs(strFileName);
            Response.Write(strFileName);
            coneccion con = new coneccion();            
            string query = "update [dbo].[evt_Docente] set estado="+activo +", [imagen]='img/" + fileImg.PostedFile.FileName + "',[grado]='" + txtGrado.Text + "',[apPaterno]='" + txtPaterno.Text + "',[apMaterno]='" + txtMaterno.Text + "',[nombres]='" + txtNombre.Text + "',[cedulaIdentidad]='" + txtCi.Text + "',[informacionDocente]='" + txtInfo.Text + "',[a_fecha_modificacion]=GETDATE(),[a_usuario_modificacion]='NUEVOUSU' WHERE [idDocente]=" + Request.QueryString["id"];            
            SqlCommand command = new SqlCommand(query, con.connection);
            con.Abrir();
            command.ExecuteReader();            
            con.Cerrar();
        }
        else
        {
            
            coneccion con = new coneccion();            
            string query = "update [dbo].[evt_Docente] set estado=" + activo + ", [grado]='" + txtGrado.Text + "',[apPaterno]='" + txtPaterno.Text + "',[apMaterno]='" + txtMaterno.Text + "',[nombres]='" + txtNombre.Text + "',[cedulaIdentidad]='" + txtCi.Text + "',[informacionDocente]='" + txtInfo.Text + "',[a_fecha_modificacion]=GETDATE(),[a_usuario_modificacion]='NUEVOUSU' WHERE [idDocente]=" + Request.QueryString["id"];            
            SqlCommand command = new SqlCommand(query, con.connection);
            con.Abrir();
            command.ExecuteReader();            
            con.Cerrar();

        }


        Response.Redirect("Docente.aspx");
    }
    public void botonAgregar() {
        btnAdd.Visible = true;
        btnUpdt.Visible = false;

    }
    public void botonModificar() {
        btnAdd.Visible = false;
        btnUpdt.Visible = true;
    }
    public bool verificaCi( string ci) {
        bool valor = false;
         coneccion con = new coneccion();
                string query = "select *from [evt_Docente]";
                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read()) {
                        if (reader.GetString(6) == ci) {
                            valor = true;
                        }
                    }
                }
                return valor;
    }
}
