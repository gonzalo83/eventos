﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;

public partial class AddEvento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            if (Request.QueryString["id"] != null)
            {
                fileImg.Attributes.Remove("required");
              
                coneccion con = new coneccion();
                string query = "select *from [evt_Evento] where idEvento=" + Request.QueryString["id"];
                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                using (SqlDataReader reader = command.ExecuteReader())
                {

                    reader.Read();
                    txtNombreE.Text = reader.GetString(1);
                    //txtNombreE.Enabled = false;
                    listaTip.SelectedValue = reader.GetString(2);
                    txtCosto.Text = reader.GetDouble(4).ToString();
                    listaModalidad.SelectedValue = reader.GetString(5);
                    txtDir.Text = reader.GetString(6);
                    txtHoras.Text = reader.GetInt32(7).ToString();
                    txtCupo.Text = reader.GetInt32(8).ToString();                    
                    txtObj.Text = reader.GetString(12);
                    txtInfo.Text = reader.GetString(11);
                    DateTime date = new DateTime(reader.GetDateTime(9).Year, reader.GetDateTime(9).Month, reader.GetDateTime(9).Day);
                    
                    this.dateI.Value = date.ToString("yyyy-MM-dd");
                    DateTime dates = new DateTime(reader.GetDateTime(10).Year, reader.GetDateTime(10).Month, reader.GetDateTime(10).Day);
                    this.dateF.Value = dates.ToString("yyyy-MM-dd");
                    if (reader.GetBoolean(13)) { EstadoCertificado.SelectedValue = "ASITENCIA"; } else { EstadoCertificado.SelectedValue = "EVALUACION"; }
                  
                }
                con.Cerrar();
                Response.Write(Session["rol"]);
                if ((Session["rol"] as string) != "administrador")
                {
                    VerificaFecha();
                }
                               
                botonModificar();
            }
            else {
                botonAgregar();
            }
        }
        
    }

    private void VerificaFecha()
    {
        try {
            coneccion conex = new coneccion();
            string query1 = "select TOP 1 [fechaInicio],horario from [dbo].[evt_Modulo] where idEvento=" + Request.QueryString["id"] + " order by [fechaInicio]";
            SqlCommand command1 = new SqlCommand(query1, conex.connection);
            conex.Abrir();
            using (SqlDataReader reader = command1.ExecuteReader())
            {
                reader.Read();

                var date = DateTime.Now;
                string datetime = DateTime.Now.ToString("hh:mm:ss tt");

                string[] subs = reader.GetString(1).Split('a');
                string[] hora = subs[0].Split(':');
                DateTime ejemploFecha = new DateTime(reader.GetDateTime(0).Year, reader.GetDateTime(0).Month, reader.GetDateTime(0).Day, Int32.Parse(hora[0]), Int32.Parse(hora[1]), 0);
                bool a = false;
                if (date > ejemploFecha)
                {
                    a = true;
                }
                if (a)
                {
                    txtNombreE.Enabled = false;
                    fileImg.Enabled = false;
                    listaTip.Enabled = false;
                    txtCosto.Enabled = false;
                    listaModalidad.Enabled = false;
                    txtDir.Enabled = false;
                    txtHoras.Enabled = false;
                    txtCupo.Enabled = false;
                    txtObj.Enabled = false;
                    txtInfo.Enabled = false;
                    this.dateI.Disabled = true;
                    this.dateF.Disabled = true;
                    EstadoCertificado.Enabled = false;
                }


            }
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        
    }


    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        try { 
            string UploadDir = Server.MapPath("~/img");
        string strFileName = Path.Combine(UploadDir, fileImg.PostedFile.FileName);
        fileImg.PostedFile.SaveAs(strFileName);
        Response.Write(strFileName);
        string fechaI = this.dateI.Value.ToString();
        string[] subs = fechaI.Split('-');
        fechaI = subs[0] + "-" + subs[2] + "-" + subs[1];
        string fechaF = this.dateF.Value.ToString();
        subs = fechaF.Split('-');
        fechaF = subs[0] + "-" + subs[2] + "-" + subs[1];
        int estado=0;
        if (EstadoCertificado.SelectedItem.Text == "ASISTENCIA") { estado = 1; }
        coneccion con = new coneccion();        
        string query = "insert into [dbo].[evt_Evento] ([nombreEvento],[tipoEvento],[imagen],[costo],[modalidad],[direccion],[cantidadHoras],[cupoMaximo],[fechaInicio],[fechaFin],objetivoGeneral,[informacionEvento],EstadoCertificado,[a_fecha_registro],[a_usuario_registro],[a_fecha_modificacion],[a_usuario_modificacion]) select '" + txtNombreE.Text + "','" + listaTip.SelectedItem.Text + "','img/" + fileImg.PostedFile.FileName + "'," + Double.Parse(txtCosto.Text) + ",'" + listaModalidad.SelectedItem.Text + "','" + txtDir.Text + "'," + Int32.Parse(txtHoras.Text) + "," + Int32.Parse(txtCupo.Text) + ",'" + fechaI + "','" + fechaF+ "','" + txtObj.Text + "','" + txtInfo.Text + "',"+ estado+ ",GETDATE(),'jose13',GETDATE(),'jose123'";
        SqlCommand command = new SqlCommand(query, con.connection);
        con.Abrir();
        command.ExecuteReader();
        
        con.Cerrar();
        Response.Redirect("Index.aspx");
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        
    }
    protected void btnUpdt_Click(object sender, EventArgs e)
    {
        try {
            if (fileImg.HasFile)
            {
                string UploadDir = Server.MapPath("~/img");
                string strFileName = Path.Combine(UploadDir, this.fileImg.PostedFile.FileName);
                fileImg.PostedFile.SaveAs(strFileName);
                Response.Write(strFileName);
                string fechaI = this.dateI.Value.ToString();
                string[] subs = fechaI.Split('-');
                fechaI = subs[0] + "-" + subs[2] + "-" + subs[1];
                string fechaF = this.dateF.Value.ToString();
                subs = fechaF.Split('-');
                fechaF = subs[0] + "-" + subs[2] + "-" + subs[1];
                int estado = 0;
                if (EstadoCertificado.SelectedItem.Text == "ASISTENCIA") { estado = 1; }
                coneccion con = new coneccion();

                string query = "update [dbo].[evt_Evento] set EstadoCertificado=" + estado + ",objetivoGeneral='" + txtObj.Text + "', [nombreEvento]='" + txtNombreE.Text + "',[tipoEvento]='" + listaTip.SelectedItem.Text + "',[imagen]='" + "img/" + this.fileImg.PostedFile.FileName + "',[costo]='" + Double.Parse(txtCosto.Text) + "',[modalidad]='" + listaModalidad.SelectedItem.Text + "',[direccion]='" + txtDir.Text + "',[cantidadHoras]='" + Int32.Parse(txtHoras.Text) + "',[cupoMaximo]='" + Int32.Parse(txtCupo.Text) + "',[fechaInicio]='" + fechaI + "',[fechaFin]='" + fechaF + "',[informacionEvento]='" + txtInfo.Text + "',[a_fecha_modificacion]=GETDATE(),[a_usuario_modificacion]='jose321' where idEvento=" + Request.QueryString["id"];

                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                command.ExecuteReader();

                con.Cerrar();
            }
            else
            {
                string fechaI = this.dateI.Value.ToString();
                string[] subs = fechaI.Split('-');
                fechaI = subs[0] + "-" + subs[2] + "-" + subs[1];
                string fechaF = this.dateF.Value.ToString();
                subs = fechaF.Split('-');
                fechaF = subs[0] + "-" + subs[2] + "-" + subs[1];
                int estado = 0;
                if (EstadoCertificado.SelectedItem.Text == "ASISTENCIA") { estado = 1; }
                coneccion con = new coneccion();

                string query = "update [dbo].[evt_Evento] set EstadoCertificado=" + estado + ",objetivoGeneral='" + txtObj.Text + "',[nombreEvento]='" + txtNombreE.Text + "',[tipoEvento]='" + listaTip.SelectedItem.Text + "',[costo]='" + Double.Parse(txtCosto.Text) + "',[modalidad]='" + listaModalidad.SelectedItem.Text + "',[direccion]='" + txtDir.Text + "',[cantidadHoras]='" + Int32.Parse(txtHoras.Text) + "',[cupoMaximo]='" + Int32.Parse(txtCupo.Text) + "',[fechaInicio]='" + fechaI + "',[fechaFin]='" + fechaF + "',[informacionEvento]='" + txtInfo.Text + "',[a_fecha_modificacion]=GETDATE(),[a_usuario_modificacion]='jose321' where idEvento=" + Request.QueryString["id"];

                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                command.ExecuteReader();

                con.Cerrar();

            }


            Response.Redirect("Index.aspx");
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        
    }
    public void botonAgregar() {
        btnAgregar.Visible = true;
        btnUpdt.Visible = false;
        lblTitulo.Text = "AGREGAR EVENTO";
    }
    public void botonModificar() {
        lblTitulo.Text = "MODIFICAR EVENTO";
        btnUpdt.Visible = true;
        btnAgregar.Visible = false;
    }
}
