﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Net.Mime;
using System.Data.SqlClient;

public partial class Formulario : System.Web.UI.Page
{
    Estudiante estudiante;
    int id_Evento;
    protected void Page_Load(object sender, EventArgs e)
    {
        Panel2.Visible = false;
        Panel3.Visible = false;

        var eventoId = Request["id"];
        //int id_Evento;
        if (!String.IsNullOrEmpty(eventoId) && int.TryParse(eventoId, out id_Evento))
        {
            var evento = Request["evento"];
            IdEvento.Value = id_Evento.ToString();
            //string nombre_evento = new Evento().nombreEvento(Convert.ToInt32(IdEvento.Value));
            Lbl_InformacionEvento.Text = "Inscribiendo en el Evento: <h4 style=\"color:Navy;\">" + evento + "</h4>";

            LinkOpciones.NavigateUrl = String.Format("./Evento.aspx?id={0}", IdEvento.Value);

            Response.Write(Session["rol"]);
            if ((Session["rol"] as string) != "administrador")
            {
                VerificaFecha();
                verificar_si_hay_cupo();
            }
        }
        else
        {
            Response.Redirect("Index.aspx");
        }

    }
    protected void Btn_Verificar_Click(object sender, EventArgs e)
    {

        Estudiante estudiante = new Estudiante(TextCI.Value);
        if (estudiante._idEstudiante > 0)//si hay id esxister el estudiante
        {
            TextIdEstudiante.Value = estudiante._idEstudiante.ToString();
            Panel2.Visible = true;
            Lbl_VerificarCI.ForeColor = Color.Green;
            Btn_Verificar.Visible = false;
            TextCI.Disabled = true;
            //preinscripcion._nuevo = false;
            Lbl_VerificarCI.Text = "Se han recuperado los datos del estudiante.";
            TextNombre.Value = estudiante._nombreCompleto;
            TextNombre.Disabled = true;
            TextEmail.Value = "freysner@gmail.com";
            TextEmail.Disabled = true;
            TextCelular.Value = estudiante._numeroCelular;
            TextCelular.Disabled = true;
            TextNacimiento.Value = "31/1/2000";
            TextNacimiento.Disabled = true;
            //CheckNuevo.Checked = false;

        }
        else
        {
            Lbl_VerificarCI.Text = "No se encuentra registrado el estudiante.";
            Lbl_VerificarCI.ForeColor = Color.Red;
        }
    }

    protected void Btn_Registrar_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        Panel2.Visible = false;
        Panel3.Visible = true;
        //primero llenar los mensajes
        string ci = TextCI.Value;
        string idEvento=IdEvento.Value;//id evento
        //string nombre=Request["evento"];
        estudiante = new Estudiante(ci);

        if (estudiante._idEstudiante > 0)
        {
            int resp=new Inscripcion().verificar(estudiante._idEstudiante, idEvento);
            if (resp==0)
            {
                string idIns=Inscribir();
                var url = String.Format("Comprobante.aspx?event={0}&student={1}&ins={2}",idEvento,estudiante._idEstudiante,idIns);
                Response.Redirect(url);
            }
            if (resp == 1)
            {
                lblmensajefinal.Text = "El estudiante ya esta inscrito.";
                lblmensajefinal.ForeColor = Color.Red;
            }
            if (resp == 2)
            {
                lblmensajefinal.Text = "No existe cupo.";
                lblmensajefinal.ForeColor = Color.Red;
            }
        }
        else
        {
            lblmensajefinal.Text="No esta registrado como estudiante, no se puede inscribir.";
            lblmensajefinal.ForeColor = Color.Red;
        }
      
    }

    public string Inscribir()
    {
        Inscripcion inscripcion = new Inscripcion();
        string mensaje="";
        string idEvento=Request["id"];
        inscripcion._idEvento = Int32.Parse(idEvento);
        inscripcion._idEstudiante = estudiante._idEstudiante;//_idEstudiante como id de preinscripcion
        inscripcion._fecha_Inscripcion = DateTime.Now;
        inscripcion._nota_final = "";
        Random objRandom = new Random();
        string codigo = string.Format("{0:X}", objRandom.Next(1000000, 9999999));
        codigo += string.Format("{0:X}", objRandom.Next(1000000, 9999999));
        inscripcion._CodigoPago = codigo;
        inscripcion._fecha_registro = inscripcion._fecha_Inscripcion;
        inscripcion._usuario_registro = "utb";
        inscripcion._fecha_modificacion = inscripcion._fecha_Inscripcion;
        inscripcion._usuario_modificacion = "utb";//+ _usuario_modificacion + "')";
        if (!inscripcion.Save())
        {
            //lblmensajefinal.Text = "Ocurrio un error.";
            //lblmensajefinal.ForeColor = Color.Red;
            mensaje = "Ocurrio un error al guardar los datos";
        }
        else
        {
            mensaje=inscripcion.idInscripcion(inscripcion._idEstudiante,idEvento);
        }
            return mensaje;

    }

    private void VerificaFecha()
    {
        try
        {
            coneccion conex = new coneccion();
            string query1 = "select TOP 1 [fechaInicio],horario from [dbo].[evt_Modulo] where idEvento=@p1 order by [fechaInicio]";
            SqlCommand command1 = new SqlCommand(query1, conex.connection);
            command1.Parameters.AddWithValue("@p1", id_Evento);
            conex.Abrir();
            using (SqlDataReader reader = command1.ExecuteReader())
            {
                reader.Read();

                var date = DateTime.Now;
                string datetime = DateTime.Now.ToString("hh:mm:ss tt");

                string[] subs = reader.GetString(1).Split('a');
                string[] hora = subs[0].Split(':');
                DateTime ejemploFecha = new DateTime(reader.GetDateTime(0).Year, reader.GetDateTime(0).Month, reader.GetDateTime(0).Day, Int32.Parse(hora[0]), Int32.Parse(hora[1]), 0);
                bool a = false;
                if (date > ejemploFecha)
                {
                    a = true;
                }
                if (a)
                {
                    Btn_Verificar.Enabled = false;
                    TextCI.Disabled=true;
                    Lbl_VerificarGlobal.Text = "La fecha Actual es mayor a la fecha de Inicio del primer modulo del Evento"
                        +"<br> No se puede inscribir<br>";
                    Lbl_VerificarGlobal.ForeColor = Color.Red;
                }


            }
        }
        catch (Exception ex) { Response.Write(ex.Message); }

    }

    protected void verificar_si_hay_cupo()
    {
        //int resp = 0;
        coneccion objConn = new coneccion();
        objConn.Abrir();
        var sql_contar = "select cupoMaximo from evt_Evento where idEvento=@p1";
        var cmd = new SqlCommand(sql_contar, objConn.connection);
        cmd.Parameters.AddWithValue("@p1", id_Evento);
        var cupo_evento = (Int32)cmd.ExecuteScalar();


        if (cupo_evento > 0)
        {
            sql_contar = "select COUNT(idInscripcion) from evt_Inscripcion where idEvento=@p1";
            cmd = new SqlCommand(sql_contar, objConn.connection);
            cmd.Parameters.AddWithValue("@p1", id_Evento);
            var numero_inscritos = (Int32)cmd.ExecuteScalar();
            if (numero_inscritos >= cupo_evento)
            {
                Btn_Verificar.Enabled = false;
                TextCI.Disabled = true;
                Lbl_VerificarGlobal.Text = "No hay cupo"
                    + "<br> No se puede inscribir<br>";
                Lbl_VerificarGlobal.ForeColor = Color.Red;
            }
        }

        objConn.Cerrar();

        //return resp;
    }
}
