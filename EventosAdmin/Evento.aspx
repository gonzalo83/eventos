﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Evento.aspx.cs" Inherits="Evento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Evento</title>
    <style>
        body, html
        {
            margin: 0;
            font-family: sans-serif;
            padding: 1rem;
        }
   #contenedor ul
        {
            padding: 0;
            width: 100%;
        }
    #contenedor li
        {
            display: inline;
        }
        #contenedor a
        {
            outline: none;
            text-decoration: none;
            display: inline-block;
            width: 200px; ;margin-right:0.625%;text-align:center;line-height:3;color:black;
        }
        #contenedor li:last-child a
        {
            margin-right: 0;
        }
        #contenedor a:link, a:visited, a:focus
        {
            background: Gainsboro;
        }
        #contenedor a:hover
        {
            background: LightGray;
        }
        #contenedor a:active
        {
            background: red;
            color: white;
        }
        .imagen
        {
            width: 200px;
        }
        #contenedor
        {
            text-align: left;
            margin: auto;
        }
        #izquierda
        {
            width: 210px;
            background-color: #999999;
            float: left;
        }
        #derecha
        {
            margin-left: 220px;
            background-color: #ffffff;
            padding: 4 4 4 4px;
        }
    </style>
</head>
<body>
    <a href="Index.aspx">Eventos</a>
    <br />
    <br />
    <form id="form1" runat="server">
        <br />

    <div id="contenedor">
        <h2>
            Evento:
            <asp:Label ID="Lbl_nombreEvento" runat="server" Text=""></asp:Label></h2>
        <div id="izquierda">
            <asp:Image ID="ImagenEvento" runat="server" class="imagen" />
        </div>
        <div id="derecha">
            Tipo de certificado: <asp:Label ID="Lbl_tipoCertificado" runat="server" 
                ForeColor="#009933"></asp:Label>
          <br />
            Fecha de Inicio: <asp:Label ID="Lbl_fechaInicio" runat="server" 
                ForeColor="#009933"></asp:Label>
            <br />
            Fecha fin: <asp:Label ID="Lbl_fechaFin" runat="server" ForeColor="#009933"></asp:Label>
            <br />
            Cupo maximo: <asp:Label ID="Lbl_cupo" runat="server" ForeColor="#009933"></asp:Label>
            <br />
            Costo: <asp:Label ID="Lbl_costo" runat="server" ForeColor="#009933"></asp:Label>
            <br />
            <div style="display:block;">
            <asp:LinkButton ID="LinkModulo" runat="server" CommandArgument="" OnClick="LinkModulo_Click" Visible="false">Modulo</asp:LinkButton>
                        <asp:LinkButton ID="LinkCarreras" runat="server" CommandArgument="" OnClick="LinkCarreras_Click" Visible="false">Carreras Organizadoras</asp:LinkButton>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkPreinscritos" runat="server" CommandArgument="" OnClick="LinkPreinscritos_Click" Visible="false">Preinscritos</asp:LinkButton>
                        <asp:LinkButton ID="LinkInscritos" runat="server" CommandArgument="" OnClick="LinkInscritos_Click" Visible="false">Inscritos</asp:LinkButton>
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkFormulario" runat="server" CommandArgument="" OnClick="LinkButton1_Click" Visible="false">Inscribir</asp:LinkButton>
               </div>

        </div>
    </div>
    </form>
</body>
</html>
