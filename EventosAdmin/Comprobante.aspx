﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Comprobante.aspx.cs" Inherits="Comprobante" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Comprobante de inscripción</title>
    <script src="js/qrious.js"></script>
    <style type="text/css">
        body
        {
        	padding: 20px;
            margin: 0px;
            font-family: Arial,Roboto;
        }
        @media print
        {
            body
            {
                padding: 0px;
                margin: 0px;
                width: 450px;
                size: 8.5in 11in;
                font-family: Arial,Roboto;
                background-color: #FFFFFF;
            }
        }
        .apretado
        {
            background-color:white;
            height:4rem;
            margin-top:-21px;
            z-index:1000;
        }
    </style>
    <script type="text/javascript">
        (qr_1 = function(valor) {
            var qr = new QRious({
                element: document.getElementById('qr'),
                value: valor,
                size: 85
            });
            //console.log(qr.toDataURL());
            //document.getElementById("Hidden1").value = qr.toDataURL();
            document.getElementById("TextB").value = qr.toDataURL();
        });

        (qr_2 = function(valor) {
            var qr2 = new QRious({
                element: document.getElementById('qr2'),
                value: valor
            });
            
            //console.log(qr2.toDataURL());
            document.getElementById("TextC").value = qr2.toDataURL();
            //debugger;
        });

        //qr_2();
        //
    function apretar()
    {
        //alert("hola mundo");
        //var form = document.forms['form1'];
        let tag=document.querySelector('#imp');
        tag.className = "apretado";
        tag.innerHTML="<button type=\"button\" disabled>Imprimir PDF</button>";
        tag.innerHTML+="<br>Generando PDF....";
        //form.submit();
        return true;
    }
</script>
</head>
<body>
    <form id="form1" runat="server" >
   
    <div>
<asp:HyperLink ID="LinkOpciones" runat="server">Ver opciones del Evento</asp:HyperLink><br />
    <h3>Revise los datos del comprobante</h3>
            <div style="padding-left: auto;">
            
          <asp:Button ID="Button1" runat="server" Text="Imprimir PDF" OnClick="Button1_Click" OnClientClick="apretar();" style="z-index:0"/><div id="imp" runat="server"></div>
         
        </div>
        <div id="imp1" style="padding-left:20px;">
            <div style="padding: 12px; margin: 12px 0 12px 0; width: 800px; border: solid 1px black;" align="center">
                <img src="img/logoutb.png"/>
                <h3>INSCRIPCIÓN A EVENTO</h3>
                <div style="width:100%" align="right">
                 Comprobante Nº:
                <asp:Label ID="Lbl_nroInscripcion" runat="server" Text=""></asp:Label></div>
                <br /><br />
                <div style="width:100%" align="left">
                Codigo:
                <asp:Label ID="Lbl_codigo" runat="server" Text=""></asp:Label>
                <br />
                Fecha de Inscripción:
                <asp:Label ID="LblFechaInscripcion" runat="server" Text=""></asp:Label>
                <br />
                Estudiante:
                <asp:Label ID="LblStudent" runat="server" Text=""></asp:Label>
                <br />
                CI:
                <asp:Label ID="LblCI" runat="server" Text=""></asp:Label>
                <br />
                <hr />
                <asp:Label ID="LblEvent" runat="server" Text=""></asp:Label>
                (<asp:Label ID="LblFecha" runat="server" Text=""></asp:Label>)
                <br />
                Modalidad:
                <asp:Label ID="LblModalidad" runat="server" Text=""></asp:Label>
                <br />
                Monto de Inscripción: <asp:Label ID="LblCosto" runat="server" Text=""></asp:Label>
                </div>
                <hr />
                <br />
                <asp:TextBox ID="TextB" runat="server" style="display:none"></asp:TextBox>
                 <asp:TextBox ID="TextC" runat="server"  style="display:none;"></asp:TextBox>    
                
                &nbsp;<img id="qr"/>
  <br/>
  
                <img id="qr2" style="display:none;"/>
  
                </div>
        </div>

    </div>

    </form>
</body>
</html>
