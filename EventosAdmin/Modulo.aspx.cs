﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Modulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
           
            if (Request.QueryString["doc"] == "N")
            {
                
                Response.Write("<script language=javascript>alert('EL CI DEL DOCENTE NO EXISTE SE DESCARTO MODULO');</script>");
                Response.Write("<script language=javascript>location.href='Modulo.aspx?id=" + Request.QueryString["id"] + "&evento=" + Request.QueryString["evento"] + "&fi=" + Request.QueryString["fi"] + "&ff=" + Request.QueryString["ff"] + "';</script>");
            }
            evt_modulo.SelectCommand = "select mo.contenido,mo.[idModulo],mo.[idEvento],mo.[idDocente],mo.[nombreModulo],convert(varchar, mo.[fechaInicio], 3) as fechaInicio,convert(varchar,mo.[fechaFin], 3)as [fechaFin],mo.[enlace],mo.[prioridadDocente],[horario],d.[cedulaIdentidad]as CarnetDocente from [dbo].[evt_Modulo] as mo inner join [dbo].[evt_Docente] as d on mo.idDocente=d.idDocente where idEvento=" + Request.QueryString["id"] + " order by idModulo desc";
            lblTitulo.Text = lblTitulo.Text + Request.QueryString["evento"];
          
            
            //Response.Write(PreviousPage.ToString()+"hol");


             
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        Response.Redirect("Preinscritos.aspx?id=" + Request.QueryString["id"]);
    }
    protected void btnInscritos_Click(object sender, EventArgs e)
    {
        Response.Redirect("Preinscritos.aspx?id=" + Request.QueryString["id"]);
    }
    protected void icono_Click(object sender, ImageClickEventArgs e)
    {

        Response.Redirect("Index.aspx");
    
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Response.Write("--"+timh.Value+"--");
    }
    protected void btnCer_Click(object sender, EventArgs e)
    {
                coneccion con = new coneccion();
                string query = "select *from [dbo].[evt_Inscripcion] as eve inner join [dbo].[Estudiante] as est on eve.idEstudiante=est.idEstudiante where idEvento=" + Request.QueryString["id"];
                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                using (SqlDataReader reader = command.ExecuteReader())
                { 
                    while(reader.Read()){
                        crearCertificado(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2),reader.GetString(4));
                    }
                }
                con.Cerrar();
    }

    private void crearCertificado(int p, int p_2, int p_3, string p_4)
    {
        try
        {
            string pathToFiles = Server.MapPath("");

            string Path = Server.MapPath("");
            string PathPDF = Server.MapPath("pdf");
            // Response.Write(":::");
            //Response.Write("("+Path+"::"+PathPDF+")");
            ReportDocument repor = new ReportDocument();
            repor.Load(Server.MapPath("reportes/certificado2.rpt"));
            repor.SetDatabaseLogon("sa", "123456", @"DESKTOP-63TTRFQ\SQLEXPRESS", "BDeventos");
            repor.SetParameterValue("@idEstudiante", p_3);
            repor.SetParameterValue("@idEvento",p_2 );
            repor.ExportToDisk(ExportFormatType.PortableDocFormat, PathPDF + "\\certificado"+p_4+".pdf");
            //Response.Write("Exito - PDF creado Correctamente");
            //Response.Write(reader.GetString(2) + "");

        }
        catch (Exception ex) { Response.Write(ex.Message); }
        //Response.Redirect("CrearPdf/PDF/" + fecha + "mes.pdf");
       // string pageurl = "pdf/registros.pdf";
        //Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");
        try {
            coneccion con = new coneccion();
        string query = "UPDATE [dbo].[evt_Inscripcion] SET certificadoPdf='pdf/certificado" + p_4 + ".pdf' where [idInscripcion]=" + p;
        SqlCommand command = new SqlCommand(query, con.connection);
        con.Abrir();
        command.ExecuteReader();        
        con.Cerrar();
        
        }catch(SqlException e){Response.Write(e.Message);
        }
        

    }

   

  
}
