﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Globalization;

public partial class AddModulo : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //verificarFecha();

        AutocompletarCiDocentes();
        if (!IsPostBack) {
            if (Request.QueryString["id"] != null)
            {
                try {
                    coneccion con = new coneccion();
                    string query = "select mo.[idModulo],mo.[idEvento],mo.[idDocente],mo.[nombreModulo],mo.[fechaInicio],mo.[fechaFin],mo.[enlace],mo.[prioridadDocente],[horario],d.[cedulaIdentidad] ,mo.contenido from [dbo].[evt_Modulo] as mo inner join [dbo].[evt_Docente] as d on mo.idDocente=d.idDocente where idModulo=" + Request.QueryString["id"];
                    SqlCommand command = new SqlCommand(query, con.connection);
                    con.Abrir();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {

                        reader.Read();
                        string s = reader.GetString(8);
                        string[] subs = s.Split('a');
                        this.timh.Value = subs[0];
                        this.timf.Value = subs[1];
                        txtCi.Text = reader.GetString(9);
                        txtnombre.Text = reader.GetString(3);
                        txtEnlace.Text = reader.GetString(6);
                        rdDocente.Checked = reader.GetBoolean(7);
                        DateTime date = new DateTime(reader.GetDateTime(4).Year, reader.GetDateTime(4).Month, reader.GetDateTime(4).Day);
                        this.dateI.Value = date.ToString("yyyy-MM-dd");
                        //this.dateI.min = "2018-01-02";
                        //this.dateI.max = "2023-01-22";
                        DateTime dates = new DateTime(reader.GetDateTime(5).Year, reader.GetDateTime(5).Month, reader.GetDateTime(5).Day);
                        this.dateF.Value = dates.ToString("yyyy-MM-dd");
                        this.Hidden1.Value = reader.GetString(10).Replace("-", string.Empty);

                    }
                    con.Cerrar();
                }
                catch (Exception ex) { Response.Write(ex.Message); }
                lblTitulo.Text = "Editar Modulo";
                

                Response.Write(Session["rol"]);
                if ((Session["rol"] as string) != "administrador")
                {
                    VerificaFecha();
                }

                   botonModificar();
            }
            else { botonAgregar(); }
          
        }

    }

    private void VerificaFecha()
    {
        try {
            coneccion conex = new coneccion();
            string query1 = "select TOP 1 [fechaInicio],horario from [dbo].[evt_Modulo] where idEvento=" + Request.QueryString["idE"] + " order by [fechaInicio]";
            SqlCommand command1 = new SqlCommand(query1, conex.connection);
            conex.Abrir();
            using (SqlDataReader reader = command1.ExecuteReader())
            {
                reader.Read();

                var date = DateTime.Now;
                string datetime = DateTime.Now.ToString("hh:mm:ss tt");

                string[] subs = reader.GetString(1).Split('a');
                string[] hora = subs[0].Split(':');
                DateTime ejemploFecha = new DateTime(reader.GetDateTime(0).Year, reader.GetDateTime(0).Month, reader.GetDateTime(0).Day, Int32.Parse(hora[0]), Int32.Parse(hora[1]), 0);
                bool a = false;
                if (date > ejemploFecha)
                {
                    a = true;
                }
                if (a)
                {

                    this.timh.Disabled = true;
                    this.timf.Disabled = true;
                    txtCi.Enabled = false;
                    txtnombre.Enabled = false;
                    txtEnlace.Enabled = false;
                    rdDocente.Enabled = false;
                    //DateTime date = new DateTime(reader.GetDateTime(4).Year, reader.GetDateTime(4).Month, reader.GetDateTime(4).Day);
                    this.dateI.Disabled = true;
                    //DateTime dates = new DateTime(reader.GetDateTime(5).Year, reader.GetDateTime(5).Month, reader.GetDateTime(5).Day);
                    this.dateF.Disabled = true;
                    this.lista.Disabled = true;
                    this.btnAdic.Disabled = true;
                    this.btnAdSub.Disabled = true;
                    //this.Hidden1.Value = reader.GetString(10).Replace("-", string.Empty);
                }


            }
        }
        catch (Exception ex) {
            Response.Write(ex.Message);
        }
        
    }

   
    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        if(!verificaDocente(txtCi.Text)){
            Response.Redirect("Modulo.aspx?id=" + Request.QueryString["idE"] + "&evento=" + Request.QueryString["evento"] + "&doc=N" + "&fi=" + Request.QueryString["fi"] + "&ff=" + Request.QueryString["ff"]);
        }
        
        int doc=0;
        if (rdDocente.Checked) {
            doc = 1;
        }

        //string hora = txtHoraini.Text + "a" + txtHorafin.Text;
        try {
            string fechaI = this.dateI.Value.ToString();
            string[] subs = fechaI.Split('-');
            fechaI = subs[0] + "-" + subs[2] + "-" + subs[1];
            string fechaF = this.dateF.Value.ToString();
            subs = fechaF.Split('-');
            fechaF = subs[0] + "-" + subs[2] + "-" + subs[1];
            string hora = this.timh.Value.ToString() + "a" + this.timf.Value.ToString();
            coneccion con = new coneccion();
            //string query = "declare @doc as int=(select idDocente from[dbo].[evt_Docente] where cedulaIdentidad='" + txtCi.Text + "') insert into [dbo].[evt_Modulo]([idEvento],[idDocente],[nombreModulo],[fechaInicio],[fechaFin],[enlace],[prioridadDocente],[horario],contenido,[a_fecha_registro],[a_usuario_registro],[a_fecha_modificacion],[a_usuario_modificacion]) select " + Int32.Parse(Request.QueryString["idE"]) + ",@doc,'" + txtnombre.Text + "','" + fi + "','" + txtAñof.Text + "-" + txtDiaf.Text + "-" + txtMesf.Text + "','" + txtEnlace.Text + "'," + doc + ",'" + hora +"','"+txtCont.Text+ "',GETDATE(),'jose',GETDATE(),'jose87'";
            string query = "declare @doc as int=(select idDocente from[dbo].[evt_Docente] where cedulaIdentidad='" + txtCi.Text + "') insert into [dbo].[evt_Modulo]([idEvento],[idDocente],[nombreModulo],[fechaInicio],[fechaFin],[enlace],[prioridadDocente],[horario],contenido,[a_fecha_registro],[a_usuario_registro],[a_fecha_modificacion],[a_usuario_modificacion]) select " + Int32.Parse(Request.QueryString["idE"]) + ",@doc,'" + txtnombre.Text + "','" + fechaI + "','" + fechaF + "','" + txtEnlace.Text + "'," + doc + ",'" + hora + "','" + this.Hidden1.Value + "',GETDATE(),'jose',GETDATE(),'jose87'";
            SqlCommand command = new SqlCommand(query, con.connection);
            con.Abrir();
            command.ExecuteReader();
            //command.Dispose();
            con.Cerrar();
            subs = this.Hidden1.Value.ToString().Split('\n');
            Response.Redirect("Modulo.aspx?id=" + Request.QueryString["idE"] + "&evento=" + Request.QueryString["evento"] + "&fi=" + Request.QueryString["fi"] + "&ff=" + Request.QueryString["ff"]);
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        
    }

    private bool verificaDocente(string p)
    {
        
        
        bool valor = false;
        coneccion con = new coneccion();
        string query = "select *from [evt_Docente]";
        SqlCommand command = new SqlCommand(query, con.connection);
        con.Abrir();
        using (SqlDataReader reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                if (reader.GetString(6) == p)
                {
                    valor = true;
                }
            }
        }
        return valor;
        
        
        
    }

    protected void btnUpdt_Click(object sender, EventArgs e)
    {
        if (!verificaDocente(txtCi.Text))
        {
            Response.Redirect("Modulo.aspx?id=" + Request.QueryString["idE"] + "&evento=" + Request.QueryString["evento"] + "&doc=N" + "&fi=" + Request.QueryString["fi"] + "&ff=" + Request.QueryString["ff"]);
        }
        int che = 0;
        if (rdDocente.Checked) {
            che = 1;
        }
        
        //string hora = txtHoraini.Text + "a" + txtHorafin.Text;


        
        string fechaI = this.dateI.Value.ToString();
        string[] subs = fechaI.Split('-');
        fechaI = subs[0] + "-" + subs[2] + "-" + subs[1];
        string fechaF = this.dateF.Value.ToString();
        subs = fechaF.Split('-');
        fechaF = subs[0] + "-" + subs[2] + "-" + subs[1];

        string hora = this.timh.Value.ToString() + "a" + this.timf.Value.ToString();

        try { 

            coneccion con = new coneccion();
        string query = "declare @doc as int=(select idDocente from[dbo].[evt_Docente] where cedulaIdentidad='" + txtCi.Text + "') update [dbo].[evt_Modulo] set contenido='" + this.Hidden1.Value + "',[idDocente]=@doc,[nombreModulo]='" + txtnombre.Text + "',[fechaInicio]='" + fechaI + "',[fechaFin]='" + fechaF + "',[enlace]='" + txtEnlace.Text + "',[prioridadDocente]=" + che + ",[horario]='" + hora + "',[a_fecha_modificacion]=GETDATE(), [a_usuario_modificacion]='USUARIOMOD' WHERE [idModulo]=" + Request.QueryString["id"];
        SqlCommand command = new SqlCommand(query, con.connection);
        con.Abrir();
        command.ExecuteReader();        
        con.Cerrar();
        lblTitulo.Text = "Agregar Modulo";
        Response.Redirect("Modulo.aspx?id=" + Request.QueryString["idE"] + "&evento=" + Request.QueryString["evento"] + "&fi=" + Request.QueryString["fi"]+"&ff="+Request.QueryString["ff"]);
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        
    }

    public void botonAgregar() {
        this.btnAgregar.Visible = true;
        this.btnUpdt.Visible = false;
    }

    public void botonModificar() {
        this.btnAgregar.Visible = false;
        this.btnUpdt.Visible = true;
    }
    public void AutocompletarCiDocentes() {
        try {
            coneccion conex = new coneccion();
            string query1 = "select *from [evt_Docente] where estado=1";
            SqlCommand command1 = new SqlCommand(query1, conex.connection);
            conex.Abrir();
            using (SqlDataReader reader = command1.ExecuteReader())
            {
                while (reader.Read())
                {
                    CiDocentes.Text = CiDocentes.Text + "<option value=" + reader.GetString(6) + ">";
                }
            }
            conex.Cerrar();
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        
    }
   
}
