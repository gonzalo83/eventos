﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;


public partial class Preinscritos : System.Web.UI.Page
{
    

    protected void Page_Load(object sender, EventArgs e)
    {

        var eventoId = Request["id"];
        int id_Evento;
        if (!String.IsNullOrEmpty(eventoId) && int.TryParse(eventoId, out id_Evento))
        {
            //evt_Preinscripcion.SelectCommand = "SELECT * FROM [evt_Preinscripcion] where idEvento="+Request.QueryString["id"];
            idEvento.Text = id_Evento.ToString();
            Lbl_Titulo.Text += " "+Request.QueryString["evento"];
            LinkOpciones.NavigateUrl = String.Format("./Evento.aspx?id={0}", idEvento.Text);

            //Response.Write(Request.QueryString["participacion"]);
            if (Request.QueryString["participacion"] == "Asistencia")
            {
                GridView1.Columns[8].Visible = false;
                GridView1.Columns[9].Visible = false;
                mensaje_asis.Visible = true;
                mensaje_eval.Visible = false;
            }
            else
            {
                mensaje_asis.Visible = false;
                mensaje_eval.Visible = true;
            }
        }
        else
        {
            Response.Redirect("Index.aspx");
        }
    }

    protected void Btn_imprimir_Click(object sender, EventArgs e)
    {
        var url = String.Format("Reporte_inscritos.aspx?id={0}&evento={1}", idEvento.Text,Lbl_Titulo.Text);
        Response.Redirect(url);
    }
    protected void Btn_certificados_Click(object sender, EventArgs e)
    {
        coneccion con = new coneccion();
        string query = "select *from [dbo].[evt_Inscripcion] as eve inner join [dbo].[Estudiante] as est on eve.idEstudiante=est.idEstudiante where idEvento=" + Request.QueryString["id"];
        SqlCommand command = new SqlCommand(query, con.connection);
        con.Abrir();
        using (SqlDataReader reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                crearCertificado(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetString(4));
            }
        }
        con.Cerrar();
    }
    private void crearCertificado(int p, int p_2, int p_3, string p_4)
    {
        DataTable tabla = new DataTable();
        try
        {

            coneccion con = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter("sp_certificado", con.connection);
            DataTable dt = new DataTable();
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            //@idEstudiante int,@idEvento int
            da.SelectCommand.Parameters.Add("@idEstudiante", SqlDbType.Int).Value = p_3;
            da.SelectCommand.Parameters.Add("@idEvento", SqlDbType.Int).Value = p_2;
            da.Fill(dt);

            tabla = dt;

            






            string pathToFiles = Server.MapPath("");

            string Path = Server.MapPath("");
            string PathPDF = Server.MapPath("pdf");            
            ReportDocument repor = new ReportDocument();
            repor.Load(Server.MapPath("reportes/certificado2.rpt"));
            repor.SetDataSource(dt);
            //repor.SetDatabaseLogon(usuario, password, servidor, "BDeventos");
            //repor.SetParameterValue("@idEstudiante", p_3);
            //repor.SetParameterValue("@idEvento", p_2);
            repor.ExportToDisk(ExportFormatType.PortableDocFormat, PathPDF + "\\certificado" + p_4 + ".pdf");
            //Response.Write("Exito - PDF creado Correctamente");
            //Response.Write(reader.GetString(2) + "");

        }
        catch (Exception ex) { Response.Write(ex.Message); }
        //Response.Redirect("CrearPdf/PDF/" + fecha + "mes.pdf");
        // string pageurl = "pdf/registros.pdf";
        //Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");
        try
        {
            if (tabla.Rows[0]["Tipocertificado"].ToString() == "Abandono")
            {
                string path = Server.MapPath("pdf\\certificado" + p_4 + ".pdf");
                coneccion con = new coneccion();
                string query = "UPDATE [dbo].[evt_Inscripcion] SET certificadoPdf=NULL where [idInscripcion]=" + p;
                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                command.ExecuteReader();
                con.Cerrar();
                File.Delete(path);
            }
            else {
                coneccion con = new coneccion();
                string query = "UPDATE [dbo].[evt_Inscripcion] SET certificadoPdf='pdf/certificado" + p_4 + ".pdf' where [idInscripcion]=" + p;
                SqlCommand command = new SqlCommand(query, con.connection);
                con.Abrir();
                command.ExecuteReader();
                con.Cerrar();
            }

            

        }
        catch (SqlException e)
        {
            Response.Write(e.Message);
        }


    }
}
