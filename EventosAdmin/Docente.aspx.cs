﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Docente : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) {
            if (Request.QueryString["id"] =="N") {
                Response.Write("<script language=javascript>alert('EL CI DEL DOCENTE YA ESTA AÑADIDO');</script>");
                Response.Write("<script language=javascript>location.href='Docente.aspx';</script>");
            }
            Response.Write(Session["rol"]);
            if ((Session["rol"] as string) != "Marketing")
            {
                // Enter correct column index.
                form1.Visible = true;
            }
            else
            {
                form1.Visible = false;
                Response.Write("Se restringio su acceso");
            }
        }
    }
    protected void btnCi_Click(object sender, EventArgs e)
    {
        evt_Docente.SelectCommand = "select *from [dbo].[evt_Docente] where cedulaIdentidad='"+txtBCi.Text+"'";
        GridDocentes.DataBind();
        icono.Visible = true;
    }
    protected void icono_Click(object sender, ImageClickEventArgs e)
    {
        evt_Docente.SelectCommand = "select *from [dbo].[evt_Docente]";
        GridDocentes.DataBind();
        icono.Visible = false;
        txtBCi.Text = null;
    }
}
