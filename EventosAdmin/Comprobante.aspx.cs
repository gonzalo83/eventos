﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Threading;
//using App_Code;

public partial class Comprobante : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //LblFechaImpresion.Text = DateTime.Now.ToShortDateString();

        //Response.Write(Server.MapPath("."));

        string Estudiante = Request.QueryString["student"];
        string Evento = Request.QueryString["event"];
        string Inscripcion = Request.QueryString["ins"];
        int idEvt = Int32.Parse(Evento);

        Estudiante student = new Estudiante(Int32.Parse(Estudiante));
        Inscripcion ins = new Inscripcion(Inscripcion);

        //datos de la inscripcion
        Lbl_nroInscripcion.Text = ins._idInscripcion.ToString("D10");
        Lbl_codigo.Text = ins._CodigoPago;
        LblFechaInscripcion.Text = ins._fecha_Inscripcion.ToShortDateString();

        //datos del Estudiante
        LblStudent.Text = student._nombreCompleto;
        LblCI.Text = student._cedulaIdentidad;

        //datos del evento
        Eventos evento1 = new Eventos(idEvt);
        //evento1.GetDatos(idEvt,"");
        LblEvent.Text = evento1._nombreEvento;
        LblFecha.Text = evento1._fecha;
        LblModalidad.Text = evento1._modalidad;
        if (evento1._costo > 0)
            LblCosto.Text = String.Format(" {0:c}", evento1._costo); //.ToString();
        else
            LblCosto.Text = "Gratuito";

        /**********DEFINIR EL DOMINIO PARA VERIFICAR LUEGO***********************/
        var url = "https://localhost:44333/Verificar.aspx?codigo=" + ins._CodigoPago;
        var valores = student._nombreCompleto + "|" + evento1._nombreEvento + "|" + ins._CodigoPago;

        //string javaScript = "qr_1('" + valores + url + "');qr_2('" + valores + url + "');";
        //string url_val = "MECARD:N:"+valores+";URL:"+url+";;";
        //********solo url por el momento para abrir sitios
        string javaScript = "qr_1('" + url + "');qr_2('" + url + "');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", javaScript, true);

        LinkOpciones.NavigateUrl = String.Format("./Evento.aspx?id={0}", Evento);


    }

    void pd_PrintPage(object sender, PrintPageEventArgs e)
    {
        float left = 60; // Borde izquierdo del área de impresión
        float top = 40;// el borde superior del área de impresión
        var Folder = System.IO.Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory);
        //Response.Write(Folder);
        System.Drawing.Image bmp = System.Drawing.Image.FromFile(Folder+@"img\logoutb.png");
        // imprimir
        e.Graphics.DrawImage(bmp, left+250, top);
        bmp.Dispose();

        // Create pen.
        Pen blackPen = new Pen(Color.Black, 3);

        // Create rectangle.
        Rectangle rect = new Rectangle(30, 30, 765, 430);

        // Draw rectangle to screen.
        e.Graphics.DrawRectangle(blackPen, rect);


        Font titlefont = new Font("Arial Black", 12);// Fuente del título
        Font font = new Font("Arial", 10);// Fuente de contenido 
        top += 60;
        e.Graphics.DrawString("INSCRIPCION A EVENTO", titlefont, Brushes.Black, left+250, top);
        top += 30;
        e.Graphics.DrawString("Comprobante Nº: " + Lbl_nroInscripcion.Text, font, Brushes.Black, left+400, top);
        top += 20;
        e.Graphics.DrawString("Codigo: "+Lbl_codigo.Text, font, Brushes.Black, left, top);
        top += 20;
        e.Graphics.DrawString("Fecha de Inscripción: " + LblFechaInscripcion.Text, font, Brushes.Black, left, top);
        top += 20;
        e.Graphics.DrawString("Estudiante: " + LblStudent.Text, font, Brushes.Black, left, top);
        top += 20;
        e.Graphics.DrawString("CI: " + LblCI.Text, font, Brushes.Black, left, top);
        //Dibuja una línea divisoria
        top += 20;
        Pen pen = new Pen(Color.DarkGray, 1);
        e.Graphics.DrawLine(pen, new Point((int)left, (int)top), new Point((int)left + (int)700, (int)top));
        //datos del evento
        top += 10;
        var titulo=LblEvent.Text+" ("+LblFecha.Text+")";
        int top_c = 80;
        var aux = "";
        int fin = titulo.Length;
        //Console.WriteLine(titulo.Length);

        while (fin > top_c)
        {
            aux = titulo.Remove(top_c + 1, fin - top_c - 1);
            titulo = titulo.Substring(top_c + 1, fin - top_c - 1);
            e.Graphics.DrawString(aux, font, Brushes.Black, left, top);
            fin = titulo.Length;
            top += 20;
        }
        e.Graphics.DrawString(titulo, font, Brushes.Black, left, top);
        top += 20;
        e.Graphics.DrawString("Modalidad: "+LblModalidad.Text, font, Brushes.Black, left, top);
        top += 20;
        e.Graphics.DrawString(LblCosto.Text, font, Brushes.Black, left, top);
        //Otra linea divisoria
        top += 20;
        e.Graphics.DrawLine(pen, new Point((int)left, (int)top), new Point((int)left + (int)700, (int)top));

        string cadena = TextB.Text.Remove(0,22);
        //Response.Write(cadena);

        byte[] decryted = Convert.FromBase64String(cadena);
        var result = Encoding.UTF8.GetString(decryted);

        string ruta_temporal = Folder + @"img\img_qr.png";

        if (File.Exists(ruta_temporal))
        {
            File.Delete(ruta_temporal);
        }

        FileStream stream = new FileStream(ruta_temporal, FileMode.CreateNew);
        BinaryWriter writer = new BinaryWriter(stream);
        writer.Write(decryted, 0, decryted.Length);
        writer.Close();

        bmp = System.Drawing.Image.FromFile(ruta_temporal);
        // imprimir
        top += 10;
        e.Graphics.DrawImage(bmp, left+250, top);
        bmp.Dispose();
        Font font_pie = new Font("Arial", 8);// Fuente de contenido 
        var mensaje = "La validez del comprobante puede verificarse escaneando el QR.";
        top += 100;
        e.Graphics.DrawString(mensaje, font_pie, Brushes.Black, left+140, top);
        mensaje = "El comprobante no tiene valor fiscal.";
        top += 15;
        e.Graphics.DrawString(mensaje, font_pie, Brushes.Black, left+140, top);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        //imp1.InnerHtml = "......";
        //ScriptManager1.RegisterAsyncPostBackControl(Button1);
        try
        {

            PrintDocument pd = new PrintDocument();
            var Folder = Server.MapPath("pdf");

            Action onCompleted = () =>
            {
                //On complete action
                pd.PrinterSettings.PrintFileName = Folder + @"\comprobante.pdf";
                pd.PrinterSettings.PrintToFile = true;
                pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                pd.Print();
                pd.Dispose();
            };
            var thread = new Thread(
              () =>
              {
                  try
                  {
                      // Do your work
                      Response.Write("");
                      if (File.Exists(Folder + @"\comprobante.pdf"))
                      {
                          File.Delete(Folder + @"\comprobante.pdf");
                      }
                      ////**************guardar el QR en la base de datos********** 
                      guardar_qr();

                  }
                  finally
                  {
                      onCompleted();
                  }
              });
            thread.Priority = ThreadPriority.Highest;
            thread.Start();
            ////***********retraso para crear el achivo fisico pdf*********
            int mydelay = 6000;
            Thread.Sleep(mydelay);

            Response.Clear();
            //Response.Buffer = true;
            //Set the appropriate ContentType.
            Response.ContentType = "Application/pdf";

            //Get the physical path to the file.
            string FilePath = MapPath("~/pdf/comprobante.pdf");
            //Write the file directly to the HTTP content output stream.
            Response.WriteFile(FilePath);
            Response.End();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            imp.InnerHtml = "No se creo el archivo PDF.Vuelva a intentarlo.";
            //Response.End();
        }
        finally
        {
            //Response.AppendHeader("Content-Disposition", "attachment; filename=foo.pdf");
        }

    }

    private void guardar_qr()
    {
        string cadena = TextC.Text.Remove(0, 22);
        
        byte[] decryted = Convert.FromBase64String(cadena);
        var result = Encoding.UTF8.GetString(decryted);

        coneccion objCon = new coneccion();
        objCon.Abrir();
        var sql = "update evt_Inscripcion set img_qr=@imagen"
            + " where idEvento=" + Request.QueryString["event"]
            + " and idEstudiante=" + Request.QueryString["student"];
        SqlCommand command = new SqlCommand(sql, objCon.connection);
        command.Parameters.AddWithValue("@imagen", decryted);
        command.ExecuteNonQuery();
        objCon.Cerrar();
    }
}
