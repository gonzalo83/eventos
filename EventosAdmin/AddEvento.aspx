﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEvento.aspx.cs" Inherits="AddEvento" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Página sin título</title>
    <style type="text/css">
     
        .style1
        {
            width: 100%;
        }
        .style2
        {
        }
        .style3
        {
            width: 351px;
        }
        .style6
        {
            width: 351px;
            height: 15px;
        }
        .style7
        {
            height: 15px;
        }
        .style8
        {
            width: 351px;
            height: 8px;
        }
        .style9
        {
            height: 8px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblTitulo" runat="server" Text="Agregar Modulo" Font-Bold="True" 
                        Font-Names="Trebuchet MS"></asp:Label>
                        </td>
                </td>
            </tr>
            <tr>
                <td class="style6">
                    <asp:Label ID="Label1" runat="server" Text="Nombre de Evento"></asp:Label>
                </td>
                <td class="style7">
                    <asp:TextBox ID="txtNombreE" runat="server" Width="157px" placeholder="Ingrese nombre de Evento"
           minlength="5" maxlength="150" required ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <asp:Label ID="Label11" runat="server" Text="Tipo de Evento"></asp:Label>
                </td>
                <td class="style9">
                    <asp:DropDownList ID="listaTip" runat="server">
                        <asp:ListItem>TALLER</asp:ListItem>
                        <asp:ListItem>SEMINARIO</asp:ListItem>
                        <asp:ListItem>CURSO</asp:ListItem>
                        <asp:ListItem>VIAJE</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style8">
                    <asp:Label ID="Label13" runat="server" Text="Tipo de Certificado"></asp:Label>
                </td>
                <td class="style9">
                    <asp:DropDownList ID="EstadoCertificado" runat="server">
                        <asp:ListItem>ASISTENCIA</asp:ListItem>
                        <asp:ListItem>EVALUACION</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label2" runat="server" Text="Imagen" Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="fileImg" runat="server" Font-Names="Trebuchet MS" required/>
                    
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label3" runat="server" Text="Costo" Font-Names="Trebuchet MS" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCosto" runat="server" Width="157px" placeholder="Ingrese Costo"
           minlength="1" maxlength="10" required pattern="^[0-9]+([,.]*[0-9]*)?$" ></asp:TextBox>
                    bs</td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label4" runat="server" Text="Modalidad" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="listaModalidad" runat="server" Font-Names="Trebuchet MS">
                        <asp:ListItem>VIRTUAL</asp:ListItem>
                        <asp:ListItem>PRESENCIAL</asp:ListItem>
                        <asp:ListItem>SEMI-PRESENCIAL</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="style3" id="carrera">
                    <asp:Label ID="Label5" runat="server" Text="Direccion" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtDir" runat="server" Width="157px" placeholder="Ingrese ubicacion"
           minlength="5" maxlength="200" required pattern="[A-Za-z0-9\s[:,#]]*"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label6" runat="server" Text="Cantidad de Horas" 
                        Font-Names="Trebuchet MS" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtHoras" runat="server" Width="157px" placeholder="Ingrese nombre de Evento"
           minlength="1" maxlength="10" required pattern="[0-9]+"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label7" runat="server" Text="Cupo Maximo" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCupo" runat="server" Width="157px" placeholder="Ingrese cantidad de cupo de estudiantes"
           minlength="1" maxlength="10" required pattern="[0-9]+"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label8" runat="server" Text="Fecha Inicio" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <input id="dateI" runat="server" type="text" required/></td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label9" runat="server" Text="Fecha Fin" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <input id="dateF" runat="server" type="text" required/></td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label10" runat="server" Text="Informacion del Evento" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtInfo" runat="server" TextMode="MultiLine" Width="157px" minlength="10" required></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label12" runat="server" Text="Objetivo General :" 
                        Font-Names="Trebuchet MS"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtObj" runat="server" TextMode="MultiLine" Width="157px" minlength="10"
                        required></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    <asp:Button ID="btnAgregar" runat="server" onclick="btnAgregar_Click" 
                        Text="Agregar" />
                    <asp:Button ID="btnUpdt" runat="server" onclick="btnUpdt_Click" 
                        Text="Modificar" />
                    <input id="Button3" onclick="jsRemoveDialog()" type="button" value="Cancelar" /></td>
            </tr>
        </table>
    
    
    </div>
    </form>
</body>
<%-- <script>
transforma()
function transforma(){
    var iDate=document.getElementById("dateI");
    iDate.type="date";
}

</script> --%>
</html>
