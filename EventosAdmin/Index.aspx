﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Index.aspx.cs" Inherits="_Index" Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administrador Eventos</title>
    <style type="text/css">
               label {
    display: block;
    font: 1rem 'Fira Sans', sans-serif;
}

input,
label {
    margin: .4rem 0;
}

        a:hover.sobre{
  color:#fff;
  background-color: black;
  
  border: 0px solid green;
  
}
               .DivPadre{
        width:100%;
        height:100%;        
        background:black;
         opacity: 0.9;
        position:absolute;
        top:0px;
        left:0px;
        z-index:30000;
        }
        .DivHijo
        {
        	background:white;
        	margin:3vh auto 0 auto;
        	padding:10px;
        	border-radius:5px;
        	}
        .style1
        {
            width: 100%;
        }
        .style3
        {
        }
        .style4
        {
            height: 231px;
        }
        .style5
        {
            height: 26px;
        }
    </style>
    <script src="js/script.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <br />
        
        <table class="style1">
            <tr>
                <td align="center" colspan="2" class="style5">
                    <asp:Label ID="lblTitulo" runat="server" Font-Bold="True" 
                        Font-Names="Trebuchet MS" Text="EVENTOS UTB"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    
                </td>
            </tr>
            <tr>
                <td class="style3" colspan="2">
                    <input id="add" type="button" value="AGREGAR" onclick="jsOpenDialog('AddEvento.aspx',480)" />
                    <%-- <input id="Button1" type="button" value="creaBoton" onclick="crearBoton();" /></td>--%>
                    <asp:TextBox ID="txtFiltro" runat="server" type="search" list="listaEventos" autocomplete="off" placeholder="Ingrese nombre del evento a buscar"
           minlength="5" maxlength="100" Width="472px"></asp:TextBox>
                    <asp:Button ID="btnFiltro" runat="server" Text="Buscar" 
                        onclick="btnFiltro_Click" />
                    <datalist id="listaEventos" ><asp:Literal ID="lstEventos" runat="server"></asp:Literal>
  <%-- <option value="Camaro DE UNO">
  <option value="Corvette">
  <option value="Impala">
  <option value="SILVICULTURA URBANA COMO MEDIDA DE MITIGACIÓN A LAS ISLAS DE CALOR URBANAS"> 
  --%>

</datalist>
<asp:ImageButton 
                        ID="icono" runat="server" ImageUrl="~/img/back.jpg" 
                        onclick="icono_Click" Width="20px" Visible="False" />
                        
                        
           
                    <asp:Button ID="btnRepReg" runat="server" onclick="btnRepReg_Click" Text="Reporte Registros" OnClientClick="desacFront('btnRepReg');"/>
                   
                    
                    &nbsp;
                    </tr>
            <tr>
                <td  colspan="2" class="style4">
                    <asp:GridView ID="GridEventos" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="idEvento,nombreEvento" DataSourceID="evt_evento" 
                        onselectedindexchanged="GridView1_SelectedIndexChanged1" 
                        Font-Names="Trebuchet MS">
                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                
                                <a class="sobre"  href="javascript:jsOpenDialog('AddEvento.aspx?id=<%# Eval("idEvento") %>',480);">Editar</a>
                                 
                                    <%-- <a class="sobre" href="Carreras.aspx?id=<%# Eval("idEvento") %>">Carreras</a>--%>
                                    <a class="sobre" href="Evento.aspx?id=<%# Eval("idEvento") %>">Opciones</a>
                                   <%--<asp:LinkButton CssClass="sobre" ID="LinkButton2" runat="server" CausesValidation="False" 
                                        CommandName="Delete" Text="Eliminar"></asp:LinkButton> --%> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="nombreEvento" HeaderText="nombreEvento" 
                                SortExpression="nombreEvento" />
                                <asp:BoundField DataField="tipoEvento" HeaderText="tipoEvento" 
                                SortExpression="tipoEvento" />
                            <asp:BoundField DataField="costo" HeaderText="costo" SortExpression="costo" />
                            <asp:BoundField DataField="modalidad" HeaderText="modalidad" 
                                SortExpression="modalidad" />
                            <asp:BoundField DataField="direccion" HeaderText="direccion" 
                                SortExpression="direccion" />
                            <asp:BoundField DataField="cantidadHoras" HeaderText="cantidadHoras" 
                                SortExpression="cantidadHoras" />
                            <asp:BoundField DataField="cupoMaximo" HeaderText="cupoMaximo" 
                                SortExpression="cupoMaximo" />
                            <asp:BoundField DataField="fechaInicio" HeaderText="fechaInicio" 
                                SortExpression="fechaInicio" />
                            <asp:BoundField DataField="fechaFin" HeaderText="fechaFin" 
                                SortExpression="fechaFin" />
                            <asp:BoundField DataField="informacionEvento" HeaderText="informacionEvento" 
                                SortExpression="informacionEvento" />
                                <asp:BoundField DataField="objetivoGeneral" HeaderText="objetivoGeneral" 
                                SortExpression="objetivoGeneral" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="evt_evento" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:Campus_db %>" 
                        DeleteCommand="DELETE FROM [evt_Evento] WHERE [idEvento] = @idEvento"
                        SelectCommand="SELECT * FROM [evt_Evento]" 
                        UpdateCommand="UPDATE [evt_Evento] SET [idCarrera] = ?, [nombreEvento] = ?, [imagen] = ?, [costo] = ?, [modalidad] = ?, [direccion] = ?, [cantidadHoras] = ?, [cupoMaximo] = ?, [fechaInicio] = ?, [fechaFin] = ?, [informacionEvento] = ?, [a_fecha_registro] = ?, [a_usuario_registro] = ?, [a_fecha_modificacion] = ?, [a_usuario_modificacion] = ? WHERE [idEvento] = ?">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="style3">
                    <%-- <a class="sobre" href="Carreras.aspx?id=<%# Eval("idEvento") %>">Carreras</a>--%>

                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
                    
<%--<asp:LinkButton CssClass="sobre" ID="LinkButton2" runat="server" CausesValidation="False" 
                                        CommandName="Delete" Text="Eliminar"></asp:LinkButton> --%>
    </div>
    </form>
</body>


<script>
    desacFront();
function desacFront(){

    //document.querySelector('#'+id).style.visibility='hidden';
    

    var elementos=document.querySelectorAll('input');
    elementos.forEach(element => {    
            element.addEventListener("click",()=>{
                //alert('elemento'+element.value);
                element.style.visibility='hidden';
            }
            )
    });


    /*if(document.getElementById('button').clicked == true)
{
   alert("button was clicked");
}*/
}
</script>

<%-- <script>







//transforma()
function transforma(){
    var iDate=document.getElementById("dateI");
    iDate.type="date";
     var iDate=document.getElementById("dateF");
    iDate.type="date";
   
}



function crearBoton(){
    
    const button = document.createElement('button'); 
    const div = document.createElement('div');
    button.type = 'button'; 
    button.innerText = 'Haz Click'; 
    div.innerHTML='hllola'+document.querySelectorAll('#divChild').length;
    div.id='divChild';    
    document.getElementById('listaDesdeC').appendChild(button); 
    document.getElementById('listaDesdeC').appendChild(div);
    const text=document.getElementById('TextBox1'); 
    //text.value=document.getElementById('listaD').value;
    text.value=text.value+document.querySelectorAll('#divChild')[document.querySelectorAll('#divChild').length-1].innerHTML;
    //text.value=text.value+document.querySelectorAll('#divChild').length;
}

 function numbersonly(e)
  {
    var unicode=e.charCode? e.charCode : e.keyCode
    if (unicode!=8 && unicode!=44)
    {
      if (unicode<48||unicode>57) //if not a number
      { return false} //disable key press    
    }  
  } 
function jsOpenDialog(url,width){
    let DivPadre=document.createElement("div");
    DivPadre.className="DivPadre";
    DivPadre.id="DivPadre";
    document.body.appendChild(DivPadre);
    let DivHijo=document.createElement("div");
    DivHijo.className="DivHijo";
    DivHijo.id="DivHijo";
    DivHijo.style.width=width+"px";
    document.body.appendChild(DivPadre);
    DivPadre.appendChild(DivHijo);
    
    
    window.scroll({
  top: 5,
  behavior: 'smooth'
});
    
    fetch(url)
    .then(function(res){
        return res.text();
    })
    .then(function (texto)
    {
        document.getElementById("DivHijo").innerHTML=texto;
        transforma();
    }
    )
    
    
}
function jsRemoveDialog(){
let DivPadre=document.getElementById("DivPadre");
document.body.removeChild(DivPadre);
}
</script>
 --%>
</html>
