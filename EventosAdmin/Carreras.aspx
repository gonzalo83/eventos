﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Carreras.aspx.cs" Inherits="Carreras" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carreras</title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
        }
        .style3
        {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    
        <table class="style1">
            <tr>
                <td align="center" class="style3" colspan="2">
                    <asp:Label ID="lblTitulo" runat="server" Text="Carreras de "></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="listaCarrera" runat="server">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="lstCarreras" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:Campus_db %>" 
                        SelectCommand="SELECT * FROM [carrera]">
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Button ID="btnAdd" runat="server" Text="Adicionar" 
                        onclick="btnAdd_Click" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    <asp:GridView ID="grdCarreras" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="carreras,idEvento,nombreCarrera" DataSourceID="BDCarreras" 
                        onrowdeleting="grdCarreras_RowDeleting" 
                        onrowdeleted="grdCarreras_RowDeleted" onrowupdated="grdCarreras_RowUpdated" 
                        onrowupdating="grdCarreras_RowUpdating">
                        <Columns>
                            <asp:CommandField ShowDeleteButton="True" 
                                DeleteText="Quitar" />
                            <asp:BoundField DataField="nombreEvento" HeaderText="nombreEvento" 
                                SortExpression="nombreEvento" />
                            <asp:TemplateField HeaderText="nombreCarrera" SortExpression="nombreCarrera">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("nombreCarrera") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="lstCarreras" 
                                        DataTextField="nombreCarrera" DataValueField="idCarrera" 
                                        SelectedValue='<%# Bind("idCarrera") %>'>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="BDCarreras" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:Campus_db %>" 
                        DeleteCommand="DELETE FROM evt_CarreraEvento WHERE [idCarrera] = @carreras and idEvento=@idEvento"
                        SelectCommand="select *from evt_CarreraEvento" 
                        UpdateCommand="UPDATE [evt_CarreraEvento] SET [nombreCarrera] = ? WHERE [idCarrera] = ?">
                       
                    </asp:SqlDataSource>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <%-- <asp:GridView ID="gridPru" runat="server">
                    </asp:GridView>--%>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    
    </div>
    </form>
</body>
</html>
