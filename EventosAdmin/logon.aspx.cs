﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;

public partial class logon : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private bool ValidateUser(string userName, string passWord)
    {
        coneccion con = new coneccion(); 
        SqlCommand cmd;
        string lookupPassword = null;

        // Check for invalid userName.
        // userName must not be null and must be between 1 and 15 characters.
        if ((null == userName) || (0 == userName.Length) || (userName.Length > 15))
        {
            System.Diagnostics.Trace.WriteLine("[ValidateUser] Input validation of userName failed.");
            return false;
        }

        // Check for invalid passWord.
        // passWord must not be null and must be between 1 and 25 characters.
        if ((null == passWord) || (0 == passWord.Length) || (passWord.Length > 25))
        {
            System.Diagnostics.Trace.WriteLine("[ValidateUser] Input validation of passWord failed.");
            return false;
        }

      
            // Consult with your SQL Server administrator for an appropriate connection
            // string to use to connect to your local SQL Server.
            con = new coneccion();
            con.Abrir();

            // Create SqlCommand to select pwd field from users table given supplied userName.
            cmd = new SqlCommand("Select pwd from users where uname=@userName", con.connection);
            cmd.Parameters.Add("@userName", SqlDbType.VarChar, 25);
            cmd.Parameters["@userName"].Value = userName;

            // Execute command and fetch pwd field into lookupPassword string.
            lookupPassword = (string)cmd.ExecuteScalar();

            // Cleanup command and connection objects.
            cmd.Dispose();
            con.Cerrar();
       

        // If no password found, return false.
        if (null == lookupPassword)
        {
            // You could write failed login attempts here to event log for additional security.
            return false;
        }

        // Compare lookupPassword and input passWord, using a case-sensitive comparison.
        return (0 == string.Compare(lookupPassword, passWord, false));
    }
    protected void Ingresar_Click(object sender, EventArgs e)
    {
        SqlCommand cmd;
        string rol = null;
        coneccion con = new coneccion();

        if (ValidateUser(txtUserName.Value, txtUserPass.Value))
        {
           /* FormsAuthenticationTicket tkt;
            string cookiestr;
            HttpCookie ck;
            tkt = new FormsAuthenticationTicket(1, txtUserName.Value, DateTime.Now,
            DateTime.Now.AddMinutes(30), chkPersistCookie.Checked, "your custom data");
            cookiestr = FormsAuthentication.Encrypt(tkt);
            ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
            if (chkPersistCookie.Checked)
                ck.Expires = tkt.Expiration;
            ck.Path = FormsAuthentication.FormsCookiePath;
            Response.Cookies.Add(ck);*/

                // Consult with your SQL Server administrator for an appropriate connection
                // string to use to connect to your local SQL Server.
                con = new coneccion();
                con.Abrir();

                // Create SqlCommand to select pwd field from users table given supplied userName.
                cmd = new SqlCommand("Select userRole from users where uname=@userName", con.connection);
                cmd.Parameters.Add("@userName", SqlDbType.VarChar, 25);
                cmd.Parameters["@userName"].Value = txtUserName.Value;

                // Execute command and fetch pwd field into lookupPassword string.
                rol = (string)cmd.ExecuteScalar();

                // Cleanup command and connection objects.
                cmd.Dispose();
                con.Cerrar();


                Session["rol"] = rol;

            string strRedirect;
            strRedirect = Request["ReturnUrl"];
            if (strRedirect == null)
                strRedirect = "Index.aspx";
            if (rol != "Estudiante")
                Response.Redirect(strRedirect, true);
            else
                Response.Redirect("https://localhost:44333/usuario/usuario.aspx", true);
        }
        else
            Response.Redirect("logon.aspx", true);

    }
}
