﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class _Index : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack){
            AutoCompletarEventos();
            evt_evento.SelectCommand = "select [idEvento],[nombreEvento],[tipoEvento],[imagen],[costo],[modalidad],[direccion],[cantidadHoras],[cupoMaximo],convert(varchar, [fechaInicio], 3) as [fechaInicio] ,convert(varchar,[fechaFin] , 3) as [fechaFin],[informacionEvento],[objetivoGeneral] from [dbo].[evt_Evento] order by idEvento DESC";            
        }

        Response.Write(Session["rol"]);
        

    }

    private void AutoCompletarEventos()
    {
        coneccion conex = new coneccion();
        string query1 = "select nombreEvento from [dbo].[evt_Evento]";
        SqlCommand command1 = new SqlCommand(query1, conex.connection);
        conex.Abrir();
        using (SqlDataReader reader = command1.ExecuteReader())
        {
            while (reader.Read())
            {
                lstEventos.Text = lstEventos.Text + "<option value=\"" + reader.GetString(0) + "\">";
                //Response.Write(reader.GetString(0));
            }
        }
        conex.Cerrar();
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*GridViewRow a = gridEventos.SelectedRow;
        string id = gridEventos.DataKeys[a.RowIndex]["idEvento"].ToString();
        GridViewRow b = gridEventos.SelectedRow;
        string ev = gridEventos.DataKeys[b.RowIndex]["nombre"].ToString();
        Response.Redirect("Asignatura.aspx?id="+id+"&e="+ev);*/
    }
    protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
    {
        GridViewRow a = GridEventos.SelectedRow;
        string id = GridEventos.DataKeys[a.RowIndex]["idEvento"].ToString();
        GridViewRow b = GridEventos.SelectedRow;
        string ev = GridEventos.DataKeys[a.RowIndex]["nombreEvento"].ToString();
        Response.Redirect("Modulo.aspx?id="+id+"&evento="+ev);
    }


    protected void btnFiltro_Click(object sender, EventArgs e)
    {
        evt_evento.SelectCommand = "select [idEvento],[nombreEvento],[tipoEvento],[imagen],[costo],[modalidad],[direccion],[cantidadHoras],[cupoMaximo],convert(varchar, [fechaInicio], 1) as [fechaInicio] ,convert(varchar,[fechaFin] , 1) as [fechaFin],[informacionEvento],[objetivoGeneral] from [dbo].[evt_Evento] where [nombreEvento]='" + txtFiltro.Text + "' order by idEvento DESC";
        GridEventos.DataBind();
        icono.Visible = true;
    }
    protected void icono_Click(object sender, ImageClickEventArgs e)
    {
        evt_evento.SelectCommand = "select [idEvento],[nombreEvento],[tipoEvento],[imagen],[costo],[modalidad],[direccion],[cantidadHoras],[cupoMaximo],convert(varchar, [fechaInicio], 1) as [fechaInicio] ,convert(varchar,[fechaFin] , 1) as [fechaFin],[informacionEvento],[objetivoGeneral] from [dbo].[evt_Evento] order by idEvento DESC";
        GridEventos.DataBind();
        icono.Visible = false;
        txtFiltro.Text = null;
    }
   /*protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Write("----"+this.dateI.Value+"----");
        string[] subs = dateI.Value.ToString().Split('-');
        Response.Write("---"+subs[0]+"-"+subs[2]+"-"+subs[1]);

        this.date.Value = dateI.Value;
    }*/
    protected void btnRepReg_Click(object sender, EventArgs e)
    {
        
        try
        {
            coneccion con = new coneccion();
            SqlDataAdapter da = new SqlDataAdapter("sp_historialEven", con.connection);
            DataTable dt = new DataTable();
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.Fill(dt);

            
            





            string pathToFiles = Server.MapPath("");
            
            string Path = Server.MapPath("");
            string PathPDF = Server.MapPath("pdf");
           // Response.Write(":::");
            //Response.Write("("+Path+"::"+PathPDF+")");
            ReportDocument repor = new ReportDocument();
            repor.Load(Server.MapPath("reportes/historialModficacion2.rpt"));
            repor.SetDataSource(dt);
            
            repor.ExportToDisk(ExportFormatType.PortableDocFormat, PathPDF + "\\registros.pdf");
            //Response.Write("Exito - PDF creado Correctamente");
            //Response.Write(reader.GetString(2) + ""); 

            
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        //Response.Redirect("CrearPdf/PDF/" + fecha + "mes.pdf");
        /*string pageurl = "pdf/registros.pdf";
        Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");*/
        string javaScript = "desactivarBoton('"+this.btnRepReg.ID+"');";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", javaScript, true);

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            string pathToFiles = Server.MapPath("");

            string Path = Server.MapPath("");
            string PathPDF = Server.MapPath("pdf");
            // Response.Write(":::");
            //Response.Write("("+Path+"::"+PathPDF+")");
            ReportDocument repor = new ReportDocument();
            repor.Load(Server.MapPath("reportes/certificado.rpt"));
            repor.SetDatabaseLogon("sa", "123456", @"DESKTOP-63TTRFQ\SQLEXPRESS", "BDeventos");
            repor.SetParameterValue("@idEstudiante", 1);
            repor.SetParameterValue("@idEvento", 2);
            
            repor.ExportToDisk(ExportFormatType.PortableDocFormat, PathPDF + "\\certificado.pdf");
            //Response.Write("Exito - PDF creado Correctamente");
            //Response.Write(reader.GetString(2) + "");         
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        //Response.Redirect("CrearPdf/PDF/" + fecha + "mes.pdf");
        string pageurl = "pdf/certificado.pdf";
        Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");
    }
}
