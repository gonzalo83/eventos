﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Inscritos.aspx.cs" Inherits="Preinscritos" %>

<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Inscritos</title>
<style type="text/css">
    a:hover.sobre
    {
        color: #fff;
        background-color: black;
        border: 0px solid green;
    }
    .DivPadre
    {
        width: 100%;
        height: 100%;
        background: black;
        opacity: 0.9;
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 30000;
    }
    .DivHijo
    {
        background: white;
        margin: 10vh auto 0 auto;
        padding: 10px;
        border-radius: 5px;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:HyperLink ID="LinkOpciones" runat="server">Ver opciones del Evento</asp:HyperLink><br /><br />

   Inscritos en: <b><asp:Label ID="Lbl_Titulo" runat="server" Text=""></asp:Label></b>
<hr />
<div id="mensaje_eval" runat="server">
    
    <p>En el evento se realiza 
    una evaluación, 
    entonces la nota final se interpreta a una escala de valores.</p>
    <ul>
    <li>de 0 a 4 es: Abandono (no se genera el certificado)</li>
    <li>de 5 a 50 es: Asistencia</li>
    <li>de 51 a 100 es: Aprobación</li>
    </ul>
</div>
<div  id="mensaje_asis" runat="server">
    <p>En el evento NO se realiza 
    una evaluación, 
    entonces todos los certificados son de asistencia.</p>
</div>
<hr />

    
&nbsp;<asp:Button ID="Btn_imprimir" runat="server" Text="Reporte Inscritos" onclick="Btn_imprimir_Click" 
         />
    <asp:Button ID="Btn_certificados" runat="server" Text="Generar Certificados" 
        onclick="Btn_certificados_Click"/>
    <asp:TextBox ID="idEvento" runat="server" Columns="5" Visible="false" />
 <br /><br />
      
                    <asp:GridView ID="GridView1" runat="server" 
            AutoGenerateColumns="False" DataKeyNames="idInscripcion"
                        DataSourceID="evt_Inscripcion">
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <span style="color: red;">No existen datos que mostrar</span>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>

                        <Columns>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <a class="sobre" href="Comprobante.aspx?event=<%# Eval("idEvento")%>&student=<%# Eval("idEstudiante")%>&ins=<%# Eval("idInscripcion")%>">
                                        Comprobante</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                            <asp:BoundField DataField="NRO" HeaderText="NRO" SortExpression="num" 
                                ReadOnly="True" />
                            <asp:BoundField DataField="fecha_Inscripcion" HeaderText="Fecha de Inscripcion" 
                                SortExpression="fechInscrip" ReadOnly="True" />
                            <asp:BoundField DataField="idEstudiante" HeaderText="Id Est." 
                                SortExpression="id" ReadOnly="True" />
                            <asp:BoundField DataField="nombreCompleto" HeaderText="Estudiante" 
                                SortExpression="estudiante" ReadOnly="True" />
                            <asp:BoundField DataField="numeroCelular" HeaderText="Celular" 
                                SortExpression="celular" ReadOnly="True" />
                            <asp:BoundField DataField="CodigoPago" HeaderText="Codigo" 
                                SortExpression="codigo" ReadOnly="True" />
                            <asp:BoundField DataField="nota_final" HeaderText="Nota Final" SortExpression="nota" />
                            <asp:BoundField DataField="Tipo_participacion" 
                                HeaderText="Tipo de Participacion" SortExpression="participacion" 
                                ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="evt_Inscripcion" runat="server" ConnectionString="<%$ ConnectionStrings:Campus_db %>"
                        DeleteCommand="DELETE FROM [evt_Inscripcion] WHERE [idInscripcion] = @idInscripcion"
                        SelectCommand="SELECT ROW_NUMBER() OVER(ORDER BY I.fecha_Inscripcion ASC) AS NRO,I.fecha_Inscripcion,I.idInscripcion,I.idEstudiante,E.nombreCompleto,E.numeroCelular,I.CodigoPago,I.nota_final,I.idEvento,
                        CASE
    WHEN I.nota_final<5 THEN 'Abandono'
	WHEN I.nota_final>=5 and I.nota_final<51 THEN 'Asistencia'
	WHEN I.nota_final>=51 THEN 'Aprobacion'
END AS Tipo_participacion

 FROM [evt_Inscripcion] I JOIN Estudiante E ON I.idEvento = @EventoActual and I.idEstudiante=E.IdEstudiante" 
              UpdateCommand="UPDATE [evt_Inscripcion] SET [nota_final] = @nota_final WHERE [idInscripcion] = @idInscripcion">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="idEvento" Name="EventoActual" PropertyName="Text" />
                        </SelectParameters>
                    </asp:SqlDataSource>       
        
    </div>
    </form>
</body>

<script type="text/javascript">
 function numbersonly(e)
  {
    var unicode=e.charCode? e.charCode : e.keyCode
    if (unicode!=8 && unicode!=44)
    {
      if (unicode<48||unicode>57) //if not a number
      { return false} //disable key press    
    }  
  } 
function jsOpenDialog(url,width){
    let DivPadre=document.createElement("div");
    DivPadre.className="DivPadre";
    DivPadre.id="DivPadre";
    document.body.appendChild(DivPadre);
    let DivHijo=document.createElement("div");
    DivHijo.className="DivHijo";
    DivHijo.id="DivHijo";
    DivHijo.style.width=width+"px";
    document.body.appendChild(DivPadre);
    DivPadre.appendChild(DivHijo);
    
    
    window.scroll({
  top: 5,
  behavior: 'smooth'
});
    
    fetch(url)
    .then(function(res){
        return res.text();
    })
    .then(function (texto)
    {
        document.getElementById("DivHijo").innerHTML=texto;
    }
    )
    
    
}
function jsRemoveDialog(){
let DivPadre=document.getElementById("DivPadre");
document.body.removeChild(DivPadre);
}
</script>

</html>
