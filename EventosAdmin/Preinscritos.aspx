﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Preinscritos.aspx.cs" Inherits="Preinscritos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Preinscritos</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <asp:HyperLink ID="LinkOpciones" runat="server">Ver opciones del Evento</asp:HyperLink><br />
        <br /><br />
        <b><asp:Label ID="Lbl_titulo" runat="server" Text="PreInscritos: "></asp:Label></b>
        <br />
        <br />
        <asp:TextBox ID="idEvento" runat="server" Columns="5" Visible="false"/>
        <asp:Label ID="Lbl_VerificarGlobal" runat="server" Text=""></asp:Label><br />
        <asp:Label ID="Lbl_mensaje" runat="server" Text=""></asp:Label>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="idPreinscripcion"
            DataSourceID="evt_Preinscripcion" OnRowCommand="GridView1_RowCommand" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black">
            <EmptyDataTemplate>
                <table>
                    <tr>
                        <td>
                            <span style="color: red;">No existen datos que mostrar</span>
                        </td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton runat="server" ID="EventoLink" Text="Terminar" CommandName="Evento"
                            CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" />
                     </ItemTemplate>
                </asp:TemplateField>
                
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:CheckBoxField DataField="nuevo" HeaderText="Es nuevo?" SortExpression="realizado" />
                <asp:BoundField DataField="fechaPreinscripcion" HeaderText="Fecha de Preinscripcion"
                    SortExpression="FechaPre" ReadOnly="True" />
                <asp:BoundField DataField="cedulaIdentidad" HeaderText="Cedula Identidad" SortExpression="cedulaIdentidad"
                    ReadOnly="True" />
                <asp:TemplateField HeaderText="Nombres" Visible="True">
                    <ItemTemplate>
                        <asp:Label ID="lblNombres" runat="server" Text='<%# Eval("nombres") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="apPaterno" HeaderText="Ap. Paterno" SortExpression="apPaterno"
                    Visible="True" ReadOnly="True" />
                <asp:BoundField DataField="apMaterno" HeaderText="Ap. Materno" SortExpression="apMaterno"
                    Visible="True" ReadOnly="True" />
                <asp:BoundField DataField="fechaNacimiento" HeaderText="Fecha de Nacimiento" SortExpression="fechaNacimiento"
                    Visible="True" ReadOnly="True" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="evt_Preinscripcion" runat="server" ConnectionString="<%$ ConnectionStrings:Campus_db %>"
            DeleteCommand="DELETE FROM [evt_Preinscripcion] WHERE [idPreinscripcion] = @idPreinscripcion"
            SelectCommand="SELECT * FROM [evt_Preinscripcion] as P 
where P.idEvento=@EventoActual and P.cedulaIdentidad not in (select cedulaIdentidad from Estudiante WHERE idEstudiante in (select idEstudiante from evt_Inscripcion WHERE idEvento=@EventoActual))"
            UpdateCommand="UPDATE [evt_Preinscripcion] SET [nuevo] = @nuevo WHERE [idPreinscripcion] = @idPreinscripcion">
            <SelectParameters>
                <asp:ControlParameter ControlID="idEvento" Name="EventoActual" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
