﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using CrystalDecisions.Shared;

public partial class Reporte : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var id=Request.QueryString["id"];
        DataTable dt = new DataTable("MiTabla");
        /*dt.Columns.Add("Code", typeof(string));
        dt.Columns.Add("Name", typeof(string));
        dt.Columns.Add("Age", typeof(string));
        dt.Rows.Add("Ejemplo", "Ejemplo", "Ejemplo");
        dt.Rows.Add("Ejemplo", "Ejemplo", "Ejemplo");
        dt.Rows.Add("Ejemplo", "Ejemplo", "Ejemplo");*/
        //dt.Columns.Add(new DataColumn("Barcode", typeof(byte[])));
        //dt.WriteXml(@"c:\temp\prueba.xml", XmlWriteMode.IgnoreSchema);
        coneccion objConn = new coneccion();
        objConn.Abrir();
       // QRCodeGenerator qrGenerator = new QRCodeGenerator();


        //SELECT * FROM [evt_Inscripcion] I JOIN Estudiante E ON I.idEvento = @EventoActual and I.idEstudiante=E.IdEstudiante

        var sql = "select cast(ROW_NUMBER() OVER(ORDER BY I.fecha_Inscripcion ASC) AS int) NRO,E.idEstudiante,E.nombreCompleto,E.numeroCelular "
        +"FROM [evt_Inscripcion] I JOIN Estudiante E ON I.idEvento ="+ id
        +" and I.idEstudiante=E.IdEstudiante";
        //Response.Write(sql);
        SqlDataAdapter da = new SqlDataAdapter(sql, objConn.connection);
        DataSet dst = new DataSet();
        da.Fill(dst);
        
        dt = dst.Tables[0];
        var sql_contar = "select COUNT(idInscripcion) from evt_Inscripcion where idEvento="+id;
        SqlCommand cmd = new SqlCommand(sql_contar, objConn.connection);
        var numero = (Int32)cmd.ExecuteScalar();
        objConn.Cerrar();
        
        ReportDocument report = new ReportDocument();
        report.Load(System.IO.Path.Combine(Server.MapPath("~/reportes/"),"inscritos.rpt"));
        report.SetDataSource(dt);
        report.SetParameterValue("titulo", "<div align='center'>" + Request.QueryString["evento"] + "</div>");
        report.SetParameterValue("total_inscritos", "Total inscritos: "+numero.ToString());
        CrystalReportViewer1.ReportSource = report;

        CrystalReportViewer1.DisplayGroupTree = false;

        string PathPDF = Server.MapPath("pdf");
        report.ExportToDisk(ExportFormatType.PortableDocFormat, PathPDF + "\\reporte_inscritos.pdf");
       
        //string pageurl = "pdf/reporte_inscritos.pdf";
        //Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");//
        //Set the appropriate ContentType.
        Response.ContentType = "Application/pdf";
        //Get the physical path to the file.
        string FilePath = MapPath("~/pdf/reporte_inscritos.pdf");
        //Write the file directly to the HTTP content output stream.
        Response.WriteFile(FilePath);
        Response.End();//*/
    }
}

