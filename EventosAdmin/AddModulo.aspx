﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddModulo.aspx.cs" Inherits="AddModulo" Debug="true"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Página sin título</title>
    
    <style type="text/css">
               .contenedor-lista{
           float: left;
       }
       

       .btnS{
        background-image:url('img/flecha.png');
        background-repeat:no-repeat;
        height:50px;
        width:25px;
        background-size: 50px 50px;
        background-position:center;
        border: none;
        background-color: transparent;
    }
    .btnB{
        background-image:url('img/flechaB.png');
        background-repeat:no-repeat;
        height:50px;
        width:25px;
        background-size: 50px 50px;
        background-position:center;
        border: none;
        
        background-color: transparent;
    }
    .btn{
        background-image:url('img/eliminar.png');
        background-repeat:no-repeat;
        height:50px;
        width:25px;
        background-size: 50px 50px;
        background-position:center;
        border: none;
        background-color: transparent;
    }
       
        .style1
        {
            width: 100%;
        }
        .style2
        {
        }
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="style1">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lblTitulo" runat="server" Text="Agregar Modulo"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label1" runat="server" Text="Cedula de Docente :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCi" runat="server" type="search" list="listamodelos" autocomplete="off" placeholder="Ingrese CI Docente"
           minlength="7" maxlength="20" required></asp:TextBox>
                    <datalist id="listamodelos"><asp:Literal ID="CiDocentes" runat="server"></asp:Literal>
  <%--<option value="Camaro">
  <option value="Corvette">
  <option value="Impala">
  <option value="Colorado"> --%>
</datalist>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label2" runat="server" Text="Nombre Modulo :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtnombre" runat="server" placeholder="Ingrese nombre de modulo"
           minlength="5" maxlength="100" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label3" runat="server" Text="fecha inicio :"></asp:Label>
                </td>
                <td>
                    <input id="dateI" runat="server" type="text" required/></td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label4" runat="server" Text="fecha fin :"></asp:Label>
                </td>
                <td>
                    <input id="dateF" runat="server" type="text" required/></td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label5" runat="server" Text="enlace :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtEnlace" runat="server" placeholder="https://chat.whatsapp.com/EX0K93cQXzW0j4IzumxMme"
           minlength="5" maxlength="100" required pattern="^http[s]?://.*"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:CheckBox ID="rdDocente" runat="server" Text="Docente Encargado" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label6" runat="server" Text="horario :"></asp:Label>
                </td>
                <td>
                 <%-- <asp:TextBox ID="txthorario" runat="server" placeholder="Horario 10:00a12:30"
           minlength="11" maxlength="100" required pattern="^[0-2][0-9]:[0-5][0-9][a][0-2][0-9]:[0-5][0-9]$" ></asp:TextBox> --%><input id="timh" runat="server" type="text" required/> a<input id="timf" 
                        runat="server" type="text" required/></td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label7" runat="server" Text="Contenido :"></asp:Label>
                </td>
                <td>
                    <%-- <asp:TextBox ID="txtCont" runat="server" placeholder="-contenido uno -contenido dos -contenido tres"
           minlength="11" maxlength="100" required 
                        pattern="^[0-2][0-9]:[0-5][0-9][a][0-2][0-9]:[0-5][0-9]$" TextMode="MultiLine" ></asp:TextBox>--%>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                 <input type="text" id="entrada">
      <input type="button" id="btnAdic" runat="server" value="Agregar Nivel" 
                        onclick="agregarNivel();">
      <input type="button" id="btnAdSub" runat="server" value="Agregar subNivel" 
                        onclick="agregarSubnivel();">      
      <br>
      <div class="contenedor-lista">
        <div class="contenedor-lista">
        <select id="lista" size="7" style="width:300px;" runat="server">
        
        </select></div>
        <div class="contenedor-lista"><input type="button" onclick="eliminar();" class="btn" ><br>
            <input type="button" onclick="subir()" class="btnS" ><br>
            <input type="button" class="btnB" onclick="bajar()"><br> </div>
          
      </div>
                    <asp:Label ID="lblSalida" runat="server" Visible="False">hola</asp:Label>
                    <input id="Hidden1" type="hidden"  runat="server"/>
                    </td>
            </tr>
        </table>
        <asp:Button ID="btnAgregar" runat="server" onclick="btnAgregar_Click" 
            Text="Agregar" />
        <asp:Button ID="btnUpdt" runat="server"  
            Text="Modificar" onclick="btnUpdt_Click" />
     <input id="Button3" onclick="jsRemoveDialog()" type="button" value="Cancelar" />
    </div>
    </form>
</body>
<script>
  

</script>
</html>
