﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Pruebas.aspx.cs" Inherits="Pruebas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Wellcome my site</h1>
        <asp:LoginStatus ID="LoginStatus1" runat="server" />
        <br />
        <asp:LoginName ID="LoginName1" runat="server" FormatString="{0} logged in." Font-Size="Medium" ForeColor="BlueViolet" />
        <br /><br />
        <asp:LoginName ID="LoginName2" runat="server" FormatString="Hi {0}!" Font-Size="XX-Large" ForeColor="Crimson" />
        <asp:LoginView ID="LoginView1" runat="server">
        </asp:LoginView>
    </div>
    </form>
</body>
</html>
