﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Docente.aspx.cs" Inherits="Docente"
    Debug="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Docente</title>
    <style type="text/css">
        body{font-family:Trebuchet MS,Arial;}
        .DivPadre
        {
            width: 100%;
            height: 100%;
            background: black;
            opacity: 0.9;
            position: absolute;
            top: 0px;
            left: 0px;
            z-index: 30000;
        }
        .DivHijo
        {
            background: white;
            margin: 10vh auto 0 auto;
            padding: 10px;
            border-radius: 5px;
        }
        .imagen
        {
            width: 75px;
            height: 55px;
        }
    </style>
    <script src="js/script.js"></script>
</head>
<body>
    <form id="form1" runat="server" visible="false">
    <div>
        <br />
        <asp:Label ID="Label1" runat="server" Text="DOCENTES DE EVENTOS" Font-Bold="True"
            Font-Names="Trebuchet MS"></asp:Label>
        <div style="padding:1rem 0;">   
        
        <asp:TextBox ID="txtBCi" runat="server" placeholder="buscaCi"></asp:TextBox>
        <asp:Button ID="btnCi" runat="server" Text="Buscar" OnClick="btnCi_Click" Font-Names="Trebuchet MS" />
        
        &nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:jsOpenDialog('AddDocente.aspx',450)"><img alt="nuevo Docente" src="img/nuevo.png" 
                style="width: 30px; height: 30px;position: relative;top: 10px; left: -5px;" />Nuevo Docente</a>
        
        <asp:ImageButton ID="icono" runat="server" ImageUrl="~/img/back.jpg" OnClick="icono_Click"
            Visible="False" Width="20px" />
        </div>
        <asp:GridView ID="GridDocentes" runat="server" AutoGenerateColumns="False" DataKeyNames="idDocente"
            DataSourceID="evt_Docente"  cellpadding="5" allowpaging="true" 
            PageSize="5" >
            <HeaderStyle 
                Font-Italic="false" 
                ForeColor="black" 
                />
            <pagersettings mode="Numeric"
          position="Bottom"           
          pagebuttoncount="10"/>
                      
        <pagerstyle
          height="30px"
          verticalalign="Bottom"
          horizontalalign="Center"/>
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <%-- <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" 
                                        CommandName="Delete" Text="Eliminar"></asp:LinkButton>--%>
                        <a href="javascript:jsOpenDialog('AddDocente.aspx?id=<%# Eval("idDocente") %>',450);">
                            Editar</a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Foto" SortExpression="imagen">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("imagen") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("imagen") %>' Visible="False"></asp:Label>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("imagen") %>' CssClass="imagen" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="grado" HeaderText="Grado Academico" SortExpression="grado" />
                <asp:BoundField DataField="apPaterno" HeaderText="Apellido Paterno" SortExpression="apPaterno" />
                <asp:BoundField DataField="apMaterno" HeaderText="Apellido Materno" SortExpression="apMaterno" />
                <asp:BoundField DataField="nombres" HeaderText="Nombres" SortExpression="nombres" />
                <asp:BoundField DataField="cedulaIdentidad" HeaderText="Cedula de Indentidad" SortExpression="cedulaIdentidad" />
                <asp:BoundField DataField="informacionDocente" HeaderText="Información del Docente"
                    SortExpression="informacionDocente" />
                <asp:CheckBoxField DataField="estado" HeaderText="estado" SortExpression="estado" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="evt_Docente" runat="server" ConnectionString="<%$ ConnectionStrings:Campus_db %>"
            DeleteCommand="DELETE FROM [evt_Docente] WHERE [idDocente] = @idDocente" SelectCommand="SELECT * FROM [evt_Docente]">
        </asp:SqlDataSource>
    </div>
    </form>
</body>
<%-- <script>
function jsOpenDialog(url,width){
    let DivPadre=document.createElement("div");
    DivPadre.className="DivPadre";
    DivPadre.id="DivPadre";
    document.body.appendChild(DivPadre);
    let DivHijo=document.createElement("div");
    DivHijo.className="DivHijo";
    DivHijo.id="DivHijo";
    DivHijo.style.width=width+"px";
    document.body.appendChild(DivPadre);
    DivPadre.appendChild(DivHijo);
    
    
    window.scroll({
  top: 5,
  behavior: 'smooth'
});
    
    fetch(url)
    .then(function(res){
        return res.text();
    })
    .then(function (texto)
    {
        document.getElementById("DivHijo").innerHTML=texto;
    }
    )
    
    
}
function jsRemoveDialog(){
let DivPadre=document.getElementById("DivPadre");
document.body.removeChild(DivPadre);
}
</script>
 --%>

</html>
