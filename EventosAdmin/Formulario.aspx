﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Formulario.aspx.cs" Inherits="Formulario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Formulario de Inscripcion</title>
    <style>
        .user-input-wrp
        {
            position: relative;
            width: 50%;
        }
        .user-input-wrp .inputText
        {
            width: 100%;
            outline: none;
            border: none;
            border-bottom: 1px solid #777;
            box-shadow: none !important;
        }
        .user-input-wrp .inputText:focus
        {
            border-color: blue;
            border-width: medium medium 2px;
        }
        .user-input-wrp .floating-label
        {
            position: absolute;
            pointer-events: none;
            top: 5px;
            left: 5px;
            font-size: 13px;
            transition: 0.2s ease all;
        }
        .user-input-wrp .inputText:focus
        .floating-label, .user-input-wrp .inputText:not(:focus):valid
        .floating-label
        {
            top: 0px;
            left: 5px;
            font-size: 15px;
            opacity: 1;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <br />
    <asp:HyperLink ID="LinkOpciones" runat="server">Ver opciones del Evento</asp:HyperLink>
        <br /><br />
        <asp:Panel ID="Panel1" runat="server">
            <h3>
                Formulario de Inscripción</h3>
                <asp:Label ID="Lbl_VerificarGlobal" runat="server" Text=""></asp:Label><br />
            <asp:HiddenField ID="IdEvento" runat="server" />
            <div class="contenedor">
                <hr />
                <asp:Label ID="Lbl_InformacionEvento" runat="server" Text=""></asp:Label>
                <hr />
            </div>
            <br />
            <div class="user-input-wrp">
                <br />
                
                <input class="inputText" required id="TextCI" runat="server" />
                
                <span class="floating-label">Cedula identidad</span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="La cedula es obligatoria"
                    ControlToValidate="TextCI" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <input id="TextIdEstudiante" runat="server" visible="false"/>
            <div class="form-group">
                <asp:Button ID="Btn_Verificar" runat="server" Text="Verificar" OnClick="Btn_Verificar_Click"
                    CssClass="btn" />
            </div>
            <p>
                <asp:Label ID="Lbl_VerificarCI" runat="server" Text=""></asp:Label></p>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <div class="user-input-wrp">
                <br />
                <input class="inputText" required id="TextEmail" runat="server" />
                <span class="floating-label">Email</span>
                <asp:RequiredFieldValidator ID="ValidatorEmail" runat="server" ErrorMessage="Su email es obligatorio"
                    ControlToValidate="TextEmail" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:Label ID="LabelErrorEmail" runat="server" Text="" AssociatedControlID="TextEmail"></asp:Label>
            </div>
            <div class="user-input-wrp">
                <br />
                <input class="inputText" required id="TextCelular" runat="server" />
                <span class="floating-label">Celular</span>
                <asp:RequiredFieldValidator ID="ValidatorCelular" runat="server" ErrorMessage="Su número de celular es obligatorio"
                    ControlToValidate="TextCelular" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <div class="user-input-wrp">
                <br />
                <input class="inputText" required id="TextNombre" runat="server" />
                <span class="floating-label">Nombre Completo</span>
                <asp:RequiredFieldValidator ID="ValidatorNombre" runat="server" ErrorMessage="Escriba su nombre"
                    ControlToValidate="TextNombre" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
                        
            <div class="user-input-wrp">
                <br />
                <input class="inputText" required id="TextNacimiento" runat="server" />
                <span class="floating-label">Fecha de Nacimiento (Dia/Mes/Año) Ej: 31/1/2000</span>
                <asp:RequiredFieldValidator ID="ValidatorNacimiento" runat="server" ErrorMessage="Su fecha de nacimiento es necesario"
                    ControlToValidate="TextNacimiento" ForeColor="Red" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextNacimiento"
                    ErrorMessage="Formato de fecha inválido" Type="Date" Operator="DataTypeCheck"
                    ForeColor="Red" SetFocusOnError="true"></asp:CompareValidator>
            </div>
            <div class="form-group">
                <asp:Button ID="Btn_Registrar" runat="server" OnClick="Btn_Registrar_Click" Text="Registrar"
                    CssClass="btn" />
            </div>
        </asp:Panel>
        <asp:Panel ID="Panel3" runat="server">
        
        <br />
            <asp:Label ID="lblmensajefinal" runat="server" Text=""></asp:Label>
           
        </asp:Panel>
    </div>
    <br />

        <br />
    </form>
</body>
</html>
