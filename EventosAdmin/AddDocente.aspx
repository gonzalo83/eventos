﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddDocente.aspx.cs" Inherits="AddDocente" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 305px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table class="style1">
            <tr>
                <td align="center" colspan="2">
                    <asp:Label ID="lbltitulo" runat="server" Text="AGREGAR DOCENTE"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label1" runat="server" Text="Foto :"></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="fileImg" runat="server" required />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label2" runat="server" Text="Grado Academico :" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtGrado" runat="server" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label3" runat="server" Text="Apellido Paterno :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPaterno" runat="server" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label4" runat="server" Text="Apellido Materno :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtMaterno" runat="server" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label5" runat="server" Text="Nombres :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtNombre" runat="server" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label6" runat="server" Text="Cedula de Identidad :"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtCi" runat="server" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label9" runat="server" Text="Docente Activo :"></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="chkD" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="style2">
                    <asp:Label ID="Label7" runat="server" Text="Informacion Docente:"></asp:Label>
                    <br />
                    <asp:Label ID="Label8" runat="server" Text="Separar la experiencia por (-)" ></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtInfo" runat="server" TextMode="MultiLine" required></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
        <asp:Button ID="btnAdd" runat="server" onclick="btnAdd_Click" Text="Agregar" />
        <asp:Button ID="btnUpdt" runat="server" onclick="btnUpdt_Click" 
            Text="Modificar" />
    <input id="Button3" onclick="jsRemoveDialog()" type="button" value="cancelar" /></td>
    </div>
    </form>
</body>

</html>
