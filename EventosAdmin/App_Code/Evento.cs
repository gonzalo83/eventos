﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Descripción breve de Eventos
/// </summary>
public class Eventos
{
    public int _idEvento { get; set; }
    public int _idCarrera { get; set; }
    public string _nombreEvento { get; set; }
    public string _tipoEvento { get; set; }
    public string _imagen { get; set; }
    public double _costo { get; set; }
    public string _modalidad { get; set; }
    public string _direccion { get; set; }
    public string _cantidadHoras { get; set; }
    public string _cupoMaximo { get; set; }
    public DateTime _fechaInicio { get; set; }
    public DateTime _fechaFin { get; set; }

    public string _fecha { get; set; }
    public string _informacionEvento { get; set; }
    public string _objetivoGeneral { get; set; }

    public Eventos()
    {
        _idEvento = 0;
    }

    public Eventos(int idEvto)
    {

        coneccion objConn = new coneccion();
        string consulta = "select * from evt_Evento where idEvento=@p1";
        objConn.Abrir();
        SqlCommand command = new SqlCommand(consulta, objConn.connection);
        command.Parameters.AddWithValue("@p1", idEvto);
        SqlDataReader reader = command.ExecuteReader();
        if (reader.Read())
        {
            //Evento(1, "Técnicas de estudio", "img/evento1.jpg",
            _idEvento = Convert.ToInt32(reader["idEvento"].ToString());
            _nombreEvento = reader["nombreEvento"].ToString();
            _tipoEvento = reader["tipoEvento"].ToString();
            _imagen = reader["imagen"].ToString();
            //fecha,costo,modalidad
            _fechaInicio = Convert.ToDateTime(reader["fechaInicio"].ToString());
            _fechaFin = Convert.ToDateTime(reader["fechaFin"].ToString());

            //_fechaInicio = Convert.ToDateTime(row["fechaInicio"].ToString());
            //_fechaFin = Convert.ToDateTime(row["fechaFin"].ToString());
            if (_fechaInicio == _fechaFin)
            {
                _fecha = _fechaInicio.ToShortDateString();
            }
            else
            {
                _fecha = _fechaInicio.ToShortDateString() + " al " + _fechaFin.ToShortDateString();
            }
            _costo = Convert.ToDouble(reader["costo"].ToString());
            _modalidad = reader["modalidad"].ToString();
            _informacionEvento = reader["informacionEvento"].ToString();
            _direccion = reader["direccion"].ToString();
            _cantidadHoras = reader["cantidadHoras"].ToString();
        }
        reader.Close();
        objConn.Cerrar();
    }
    public Eventos(int id, string nombre, string imagen, string objetivo)
    {
        _idEvento = id;
        _nombreEvento = nombre;
        _objetivoGeneral = objetivo;
    }

}
