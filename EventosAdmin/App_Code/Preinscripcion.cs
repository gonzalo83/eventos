﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Descripción breve de Preinscripcion
/// </summary>

    public class Preinscripcion
    {
        public int _idPreinscripcion { get; set; }
        public int _idEvento { get; set; }
        public DateTime _fechaPreinscripcion { get; set; }
        public string _cedulaIdentidad { get; set; }
        public string _nombres { get; set; }
        public string _apPaterno { get; set; }
        public string _apMaterno { get; set; }
        public DateTime _fechaNacimiento { get; set; }
        public string _numeroCelular { get; set; }
        public string _correo { get; set; }

        public bool _nuevo { get; set; }
        public string _codigo_pago { get; set; }
        public bool _pago_realizado { get; set; }

        public Preinscripcion()
        {
            //
            // TODO: Agregar aquí la lógica del constructor
            //
        }
        public bool  Save()
        {
            string campos = "idEvento,fechaPreinscripcion,cedulaIdentidad,nombres,apPaterno,"
            +"apMaterno,fechaNacimiento,numeroCelular,correo,nuevo,codigo_pago";
            string sql = "insert into evt_Preinscripcion(" + campos + ") values ("
            //coneccion objConn = new coneccion();
             + _idEvento + ",'"
             + _fechaPreinscripcion + "','"
            + _cedulaIdentidad + "','"
            + _nombres + "','"
            + _apPaterno + "','"
            + _apMaterno + "','"
            + _fechaNacimiento + "','"
            + _numeroCelular + "','"
            + _correo + "',";
            sql += _nuevo.Equals(true) ? 1 : 0;
            sql += ",'"+ _codigo_pago + "')";
            //sql += _correo + "')";
            try
            {
                Ejecutar(sql);
                return true;
            }
            catch
            {
                return false;
            }
            //return sql;
        }

        public bool RecuperarDatos(string ci)
        {
            if (ci == "4780415")
            {
                return true;

            }
            return false;
        }

        public void Ejecutar(string sql)
        {
            coneccion objCon = new coneccion();
            SqlCommand command = new SqlCommand(sql, objCon.connection);
            //command.Connection.Open();
            command.ExecuteNonQuery();
            objCon.Cerrar();
        }
    }

