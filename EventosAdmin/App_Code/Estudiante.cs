﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

    public class Estudiante
    {
        public int _idEstudiante;
        public string _cedulaIdentidad;
        public string _nombreCompleto;
        public string _numeroCelular;
        public Estudiante() 
        { 
        
        }
        public Estudiante(string ci)
        {
            coneccion objConn = new coneccion();
            string consulta = "select * from estudiante where cedulaIdentidad=@p1";
            objConn.Abrir();
            SqlCommand command = new SqlCommand(consulta, objConn.connection);
            command.Parameters.AddWithValue("@p1", ci);
            SqlDataReader reader = command.ExecuteReader();
            //int idEstudiante = -1;
            if (reader.Read())
            {
                _idEstudiante = Convert.ToInt32(reader["idEstudiante"].ToString());
                _cedulaIdentidad = reader["cedulaIdentidad"].ToString();
                _nombreCompleto= reader["nombreCompleto"].ToString();
                _numeroCelular = reader["numeroCelular"].ToString();
            }

            reader.Close();
            objConn.Cerrar();
        }
        public Estudiante(int idEstudiante)
        {
            coneccion objConn = new coneccion();
            string consulta = "select * from estudiante where idEstudiante=@p1";
            objConn.Abrir();
            SqlCommand command = new SqlCommand(consulta, objConn.connection);
            command.Parameters.AddWithValue("@p1", idEstudiante);
            SqlDataReader reader = command.ExecuteReader();
            //int idEstudiante = -1;
            if (reader.Read())
            {
                _idEstudiante = Convert.ToInt32(reader["idEstudiante"].ToString());
                _cedulaIdentidad = reader["cedulaIdentidad"].ToString();
                _nombreCompleto = reader["nombreCompleto"].ToString();
                _numeroCelular = reader["numeroCelular"].ToString();
            }

            reader.Close();
            objConn.Cerrar();
        }
    }
