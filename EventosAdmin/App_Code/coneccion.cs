﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Descripción breve de coneccion
/// </summary>
public class coneccion
{
    public string res;
    public SqlConnection connection;
    public coneccion()
    {
        try
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Campus_db"].ConnectionString);
            //Console.WriteLine("coneccion a base de datos");
            res = "coneccion base de datos";
            //Console.WriteLine("========================");

        }
        catch (Exception ex)
        {
            //Console.WriteLine(ex.ToString());
            res = ex.ToString();
            
        }
    }
    public void Cerrar()
    {
        if (connection.State == System.Data.ConnectionState.Open)
        {
            connection.Close();
        }

    }
    public SqlConnection Abrir()
    {
        try
        {
            connection.Open();
        }
        catch (Exception ex)
        {
            res = ex.Message;   
        }
        return connection;
    }

}
