﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;


public class Inscripcion
{
    public int _idInscripcion { get; set; }
    public int _idEvento { get; set; }
    public int _idEstudiante { get; set; }
    public DateTime _fecha_Inscripcion { get; set; }
    public string _nota_final { get; set; }
    public string _CodigoPago { get; set; }
    public DateTime _fecha_registro { get; set; }
    public string _usuario_registro { get; set; }
    public DateTime _fecha_modificacion { get; set; }
    public string _usuario_modificacion { get; set; }

    public Inscripcion()
    { 
    }

    public bool Save()
    {
        try
        {
            coneccion objCon = new coneccion();
            objCon.Abrir();
            string campos = "idEvento,idEstudiante,fecha_Inscripcion,CodigoPago,nota_final,"
            + "a_fecha_registro,a_usuario_registro,a_fecha_modificacion,a_usuario_modificacion";

            string sql = "insert into evt_Inscripcion(" + campos + ")"
            + "values (@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9)";


            SqlCommand command = new SqlCommand(sql, objCon.connection);
            command.Parameters.AddWithValue("@p1", _idEvento);
            command.Parameters.AddWithValue("@p2", _idEstudiante);
            command.Parameters.AddWithValue("@p3", _fecha_Inscripcion);
            command.Parameters.AddWithValue("@p4", _CodigoPago);
            command.Parameters.AddWithValue("@p5", _nota_final);
            command.Parameters.AddWithValue("@p6", _fecha_registro);
            command.Parameters.AddWithValue("@p7", _usuario_registro);
            command.Parameters.AddWithValue("@p8", _fecha_modificacion);
            command.Parameters.AddWithValue("@p9", _usuario_modificacion);
            command.ExecuteNonQuery();
            objCon.Cerrar();
            return true;
        }
        catch
        {
            return false;
        }
        
    }
    public int verificar(int idEstudiante, string idEvento)
    {
        //resp=0 se puede inscribir
        int resp = 0;
        //resp=1 ya esta inscrito
        resp = verificar_si_esta_inscrito(idEstudiante,idEvento);
        return resp;
    }
    protected int verificar_si_esta_inscrito(int idEstudiante, string idEvento)
    {
        coneccion objConn = new coneccion();
        string consulta = "select * from evt_Inscripcion where idEvento="
            + Int32.Parse(idEvento) + " and idEstudiante=" + idEstudiante;
        objConn.Abrir();
        SqlCommand command = new SqlCommand(consulta, objConn.connection);

        SqlDataReader reader = command.ExecuteReader();
        int resp = 0;
        if (reader.Read())
        {
            resp = 1;
        }
        reader.Close();
        objConn.Cerrar();
        return resp;
    }
   


    public string idInscripcion(int idEstudiante, string idEvento)
    {

        coneccion objConn = new coneccion();
        string consulta = "select idInscripcion from evt_Inscripcion where idEvento="
            + Int32.Parse(idEvento) + " and idEstudiante=" + idEstudiante;
        objConn.Abrir();
        //var sql_contar = "select COUNT(idInscripcion) from evt_Inscripcion where idEvento=" + id;
        SqlCommand cmd = new SqlCommand(consulta, objConn.connection);
        int id = (Int32)cmd.ExecuteScalar();
        objConn.Cerrar();
        //reader.Close();
        objConn.Cerrar();
        return id.ToString();
    }
    public Inscripcion(string id)
    {

        coneccion objConn = new coneccion();
        string consulta = "select * from evt_Inscripcion where idInscripcion="+ id;
        objConn.Abrir();
        SqlCommand command = new SqlCommand(consulta, objConn.connection);

        SqlDataReader reader = command.ExecuteReader();
        if (reader.Read())
        {
            _idInscripcion = Int32.Parse(reader["idInscripcion"].ToString());
            _CodigoPago = reader["CodigoPago"].ToString();
            _fecha_Inscripcion = Convert.ToDateTime(reader["fecha_Inscripcion"].ToString());
        }
        reader.Close();
        objConn.Cerrar();
        //return ins;
    }
}
