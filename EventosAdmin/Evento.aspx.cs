﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Evento : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int id = 0;
        bool tipo_certificado = false;
        var eventoId = Request["id"];
        if (!String.IsNullOrEmpty(eventoId) && int.TryParse(eventoId, out id))
        {
            //id = Convert.ToInt32(Request.QueryString["id"]);

            coneccion conexion = new coneccion();
            conexion.Abrir();
            //string cod = textBox1.Text;
            string cadena = "select nombreEvento,imagen,fechaInicio,fechaFin,cupoMaximo,costo,EstadoCertificado from evt_Evento where idEvento=@p1";
            SqlCommand comando = new SqlCommand(cadena, conexion.connection);
            comando.Parameters.AddWithValue("@p1", id);
            SqlDataReader registro = comando.ExecuteReader();
            if (registro.Read())
            {
                Lbl_nombreEvento.Text = registro["nombreEvento"].ToString();
                Lbl_fechaInicio.Text = Convert.ToDateTime(registro["fechaInicio"]).ToShortDateString();
                Lbl_fechaFin.Text = Convert.ToDateTime(registro["fechaFin"]).ToShortDateString();
                ImagenEvento.ImageUrl = registro["imagen"].ToString();
                LinkModulo.CommandArgument = id.ToString();
                LinkInscritos.CommandArgument = id.ToString();
                LinkPreinscritos.CommandArgument = id.ToString();
                LinkFormulario.CommandArgument = id.ToString();
                LinkCarreras.CommandArgument = id.ToString();
                Lbl_cupo.Text += registro["cupoMaximo"].ToString();
                Lbl_costo.Text += registro["costo"].ToString();
                tipo_certificado = registro["EstadoCertificado"].Equals(true) ? true : false;
                if (tipo_certificado)
                    Lbl_tipoCertificado.Text = "Asistencia";
                else
                    Lbl_tipoCertificado.Text = "Evaluación";
            }
            //else
            //  MessageBox.Show("No existe un artículo con el código ingresado");
            conexion.Cerrar();
            Response.Write(Session["rol"]);
            verificar_usuario();
        }
        else
        {
            Response.Redirect("Index.aspx");
        }


    }
    protected void verificar_usuario()
    {
        if ((Session["rol"] as string) == "administrador")
        {
            //Response.Write("hola mundo");
            LinkModulo.Visible = true;
            LinkCarreras.Visible = true;
            LinkPreinscritos.Visible = true;
            LinkInscritos.Visible = true;
            LinkFormulario.Visible = true;
        }
        if ((Session["rol"] as string) == "Director de Carrera")
        {
            LinkModulo.Visible = true;
            LinkCarreras.Visible = true;
            LinkPreinscritos.Visible = true;
            LinkInscritos.Visible = true;
            LinkFormulario.Visible = true;
        }
        if ((Session["rol"] as string) == "Marketing")
        {
            LinkPreinscritos.Visible = true;
            LinkInscritos.Visible = true;
            LinkFormulario.Visible = true;
        }

    }

    protected void LinkModulo_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string entrada = btn.CommandArgument.ToString();
        // formato datatime
        DateTime DateObject = Convert.ToDateTime(Lbl_fechaInicio.Text);
        DateTime fech_ini = new DateTime(DateObject.Year, DateObject.Month, DateObject.Day);
        DateObject = Convert.ToDateTime(Lbl_fechaFin.Text);
        DateTime fech_fin = new DateTime(DateObject.Year, DateObject.Month, DateObject.Day);

        string enl = String.Format("Modulo.aspx?id={0}&evento={1}&fi={2}&ff={3}", Request.QueryString["id"], Lbl_nombreEvento.Text, fech_ini.ToString("yyyy-MM-dd"), fech_fin.ToString("yyyy-MM-dd"));
        //formato fechas 2018-06-12
        Response.Redirect(enl);
    }
    /* protected void LinkInscritos_Click(object sender, EventArgs e)
     {
         LinkButton btn = (LinkButton)sender;
         string entrada = btn.CommandArgument.ToString();
         Response.Redirect("Inscritos.aspx?id=" + entrada + "&evento=" + Lbl_nombreEvento.Text);
     }*/
    protected void LinkPreinscritos_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string entrada = btn.CommandArgument.ToString();
        var url = String.Format("Preinscritos.aspx?id={0}&evento={1}", Request.QueryString["id"], Lbl_nombreEvento.Text);
        Response.Redirect(url);
    }
    protected void LinkCarreras_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string entrada = btn.CommandArgument.ToString();
        var url = String.Format("Carreras.aspx?id={0}&evento={1}", Request.QueryString["id"], Lbl_nombreEvento.Text);
        Response.Redirect(url);
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string entrada = btn.CommandArgument.ToString();
        var url = String.Format("Formulario.aspx?id={0}&evento={1}", Request.QueryString["id"], Lbl_nombreEvento.Text);
        Response.Redirect(url);
    }
    protected void LinkInscritos_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        string entrada = btn.CommandArgument.ToString();
        var url = String.Format("Inscritos.aspx?id={0}&evento={1}&participacion={2}", Request.QueryString["id"], Lbl_nombreEvento.Text, Lbl_tipoCertificado.Text);
        Response.Redirect(url);
    }
    protected void btnRepReg_Click(object sender, EventArgs e)
    {

        try
        {
            string pathToFiles = Server.MapPath("");

            string Path = Server.MapPath("");
            string PathPDF = Server.MapPath("pdf");
            // Response.Write(":::");
            //Response.Write("("+Path+"::"+PathPDF+")");
            ReportDocument repor = new ReportDocument();
            repor.Load(Server.MapPath("historialModificacion.rpt"));
            repor.SetDatabaseLogon("sa", "123456", @"DESKTOP-CVV2QP0\SQLEXPRESS", "BDeventos");
            repor.ExportToDisk(ExportFormatType.PortableDocFormat, PathPDF + "\\registros.pdf");
            //Response.Write("Exito - PDF creado Correctamente");
            //Response.Write(reader.GetString(2) + "");         
        }
        catch (Exception ex) { Response.Write(ex.Message); }
        //Response.Redirect("CrearPdf/PDF/" + fecha + "mes.pdf");
        string pageurl = "pdf/registros.pdf";
        Response.Write("<script> window.open('" + pageurl + "','_blank'); </script>");

    }
}
